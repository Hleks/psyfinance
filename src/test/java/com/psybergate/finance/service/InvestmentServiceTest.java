package com.psybergate.finance.service;

import static com.psybergate.finance.util.BigDecimalUtil.ZERO;
import static com.psybergate.finance.util.BigDecimalUtil.convert;
import static com.psybergate.finance.util.BigDecimalUtil.percent;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.psybergate.finance.domain.investment.Investment;
import com.psybergate.finance.service.investment.InvestmentService;
import com.psybergate.finance.service.investment.InvestmentServiceImpl;

public class InvestmentServiceTest {

	private Investment investment;

	private InvestmentService service;

	@Before
	public void setup() {
		service = new InvestmentServiceImpl();
		investment = new Investment(ZERO, percent(22.4), convert(1000), 60);
	}

	@Test
	public void deposit() {
		service.deposit(investment, 12, 50_000);

		assertEquals(convert(64_493.79), investment.getMonths().get(11).getClosing());
		assertEquals(convert(66_716.34), investment.getMonths().get(12).getClosing());
	}

	@Test
	public void deposits() {
		service.deposit(investment, 12, 50_000);
		service.deposit(investment, 12, 1_000);
		service.deposit(investment, 12, 20_000);

		assertEquals(convert(85_885.79), investment.getMonths().get(11).getClosing());
		assertEquals(convert(88_507.66), investment.getMonths().get(12).getClosing());
	}

	@Test
	public void withdraw() {
		service.deposit(investment, 12, 50_000);
		service.withdraw(investment, 25, 60_000);

		assertEquals(convert(35_735.17), investment.getMonths().get(24).getClosing());
		assertEquals(convert(37_420.89), investment.getMonths().get(25).getClosing());
	}

	@Test
	public void withdrawals() {
		service.deposit(investment, 12, 50_000);
		service.withdraw(investment, 25, 60_000);
		service.withdraw(investment, 25, 4_000);
		service.withdraw(investment, 25, 800);

		assertEquals(convert(30_845.57), investment.getMonths().get(24).getClosing());
		assertEquals(convert(32_440.02), investment.getMonths().get(25).getClosing());
	}

	@Test
	public void depositAndWithdrawal() {
		service.deposit(investment, 12, 50_000);
		service.deposit(investment, 25, 60_000);
		service.withdraw(investment, 25, 4_000);
		service.withdraw(investment, 25, 2_000);

		assertEquals(convert(151_863.17), investment.getMonths().get(24).getClosing());
		assertEquals(convert(155_716.62), investment.getMonths().get(25).getClosing());
	}

	@Test
	public void checkNominalRateForMonthsAfterRateChange() {
		service.setNominalRateFor(investment, 4, percent(25));
		assertEquals(percent(22.4), investment.getNominalRateFor(3));
		assertEquals(percent(25), investment.getNominalRateFor(4));
		assertEquals(percent(25), investment.getNominalRateFor(5));
	}

	@Test
	public void checkClosingForMonthsAfterRateChange() {
		service.setNominalRateFor(investment, 4, percent(25));
		assertEquals(convert(3113.40), investment.getClosingBalanceFor(3));
		assertEquals(convert(4199.10), investment.getClosingBalanceFor(4));
		assertEquals(convert(5307.41), investment.getClosingBalanceFor(5));
	}
}
