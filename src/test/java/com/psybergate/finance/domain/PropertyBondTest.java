package com.psybergate.finance.domain;

import static com.psybergate.finance.util.BigDecimalUtil.convert;
import static com.psybergate.finance.util.BigDecimalUtil.percent;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.Before;
import org.junit.Test;

import com.psybergate.finance.PsyfinanceException;
import com.psybergate.finance.domain.propertybond.PropertyBond;

public class PropertyBondTest {

	private static final BigDecimal ZERO = BigDecimal.ZERO;

	private PropertyBond propertyBond;

	@Before
	public void setup() {
		propertyBond = createPropertyBond(650_000, 0.12, 240, 0, 7157.06, 5000, 10_000);
	}

	@Test
	public void creationNotAllowedWhenNominalRateIsNull() {
		assertInvalidForCreation(convert(750_000), null, 240, ZERO, convert(7157.06), convert(5000), convert(10_000),
				"Creation not be allowed when nominal rate is null");
	}

	@Test
	public void creationNotAllowedWhenMonthlyPaymentIsNull() {
		assertInvalidForCreation(convert(750_000), percent(12), 240, convert(0), null, convert(5000),
				BigDecimal.valueOf(10_000), "Creation not be allowed when monthly payment is null");
	}

	@Test
	public void creationNotAllowedWhenBondCostsIsNull() {
		assertInvalidForCreation(convert(750_000), percent(12), 240, convert(0), convert(7157.06), null, convert(10_000),
				"Creation not be allowed when bond costs are null");
	}

	@Test
	public void creationNotAllowedWhenLegalCostsIsNull() {
		assertInvalidForCreation(convert(750_000), percent(12), 240, convert(0), convert(7157.06), convert(5000), null,
				"Creation not be allowed when legal costs are null");
	}

	@Test
	public void creationNotAllowedWhenTermInMonthsIsLessThanZero() {
		assertInvalidForCreation(convert(750_000), percent(12), -1, convert(0), convert(7157.06), convert(5000),
				convert(10_000), "Creation not be allowed when term in months is less than 0");
	}

	@Test
	public void creationNotAllowedWhenPurchasePriceIsZeroOrLess() {
		assertInvalidForCreation(convert(-1), BigDecimal.valueOf(0.12), 240, convert(0), convert(7157.06), convert(5000),
				convert(10_000), "Creation not be allowed when purchase price is 0");
	}

	@Test
	public void creationNotAllowedWhenNominalRateIsZeroOrLess() {
		assertInvalidForCreation(convert(-1), percent(-1), 240, convert(0), convert(7157.06), convert(5000),
				convert(10_000), "Creation not be allowed when purchase price is 0");
	}

	@Test
	public void getPrincipleForInvalidMonthNumNotAllowed() {
		PsyfinanceTestingUtil.assertMonthNumInvalidForMethod("getPrincipalFor", propertyBond, 240);
	}

	@Test
	public void getInterestForInvalidMonthNumNotAllowed() {
		PsyfinanceTestingUtil.assertMonthNumInvalidForMethod("getInterestFor", propertyBond, 240);
	}

	@Test
	public void getClosingForInvalidMonthNumNotAllowed() {
		PsyfinanceTestingUtil.assertMonthNumInvalidForMethod("getClosingFor", propertyBond, 240);
	}

	@Test
	public void getCumalitiveInterestUpToInvalidMonthNumNotAllowed() {
		PsyfinanceTestingUtil.assertMonthNumInvalidForMethod("getCumalitiveInterestUpTo", propertyBond, 240);
	}

	@Test
	public void getRepaymentForInvalidMonthNumNotAllowed() {
		PsyfinanceTestingUtil.assertMonthNumInvalidForMethod("getRepaymentFor", propertyBond, 240);
	}

	@Test
	public void getOutstandingPrincipalForInvalidMonthNumNotAllowed() {
		PsyfinanceTestingUtil.assertMonthNumInvalidForMethod("getOutstandingPrincipalFor", propertyBond, 240);
	}

	@Test
	public void totalBondCostMustBeGreaterThanZero() {
		assertTrue(propertyBond.getTotalBondCost().compareTo(convert(0)) > 0);
	}

	@Test
	public void totalBondCostCalculatesCorrectly() {
		PropertyBond propertyBond = createPropertyBond(750_000, 0.12, 240, 0.1, 7157.06, 5000, 10_000);
		PsyfinanceTestingUtil.assertMonetaryAmountsEqual(1717694.40, propertyBond.getTotalBondCost());
	}

	@Test
	public void totalBondCostMustBeGreaterThanPurchasePrice() {
		BigDecimal totalBondCost = propertyBond.getTotalBondCost();
		BigDecimal purchasePrice = propertyBond.getPurchasePrice();
		boolean bondCostGreaterThanPurchasePrice = totalBondCost.compareTo(purchasePrice) > 0;
		assertTrue(bondCostGreaterThanPurchasePrice);
	}

	@Test
	public void assertTransferTaxCorrect() {
		assertCorrectTransferTax(900_000, 0);
		assertCorrectTransferTax(1_000_000, 3000);
		assertCorrectTransferTax(1_250_000, 10500);
		assertCorrectTransferTax(1_750_000, 40500);
		assertCorrectTransferTax(2_250_000, 80500);
		assertCorrectTransferTax(10_000_000, 933_000);
		assertCorrectTransferTax(10_000_001, 933_000.13);
	}

	@Test
	public void getMinimumRequiredRepaymentTest() {
		PropertyBond propertyBond = createPropertyBond(1_000_000, 0.125, 240, 0, 7157.06, 5000, 10_000);
		propertyBond.useMinimumRepayment(1, 240);
//		assertEquals(convert(11361.41), propertyBond.getMinimumRequiredRepayment(1));
//		assertEquals(convert(11361.41), propertyBond.getMinimumRequiredRepayment(2));
//		assertEquals(convert(11361.40), propertyBond.getMinimumRequiredRepayment(12));
	}

	private void assertCorrectTransferTax(double purchasePrice, double expectedTransferTax) {
		PropertyBond propertyBond = createPropertyBond(purchasePrice, 0.12, 240, 0, 7157.06, 5000, 10_000);
		PsyfinanceTestingUtil.assertMonetaryAmountsEqual(expectedTransferTax, propertyBond.getTransferTax());
	}

	private PropertyBond createPropertyBond(BigDecimal purchasePrice, BigDecimal nominalRate, int termInMonth,
			BigDecimal initialDeposit, BigDecimal monthlyRepayment, BigDecimal bondCosts, BigDecimal legalCosts) {
		return new PropertyBond(nominalRate, purchasePrice, initialDeposit, bondCosts, legalCosts, monthlyRepayment,
				termInMonth);
	}

	private PropertyBond createPropertyBond(double purchasePrice, double nominalRate, int termInMonth,
			double initialDeposit, double monthlyRepayment, double bondCosts, double legalCosts) {
		return new PropertyBond(nominalRate, purchasePrice, initialDeposit, bondCosts, legalCosts, monthlyRepayment,
				termInMonth);
	}

	private void assertInvalidForCreation(BigDecimal purchasePrice, BigDecimal nominalRate, int termInMonth,
			BigDecimal initialDeposit, BigDecimal monthlyRepayment, BigDecimal bondCosts, BigDecimal legalCosts,
			String message) {
		try {
			createPropertyBond(purchasePrice, nominalRate, termInMonth, initialDeposit, monthlyRepayment, bondCosts,
					legalCosts);
			fail(message);
		} catch ( PsyfinanceException | NullPointerException expected ) {
			// Test passed
		}
	}

	@Test
	public void legalFeesCalculatesAsExpected() {
		propertyBond = createPropertyBond(convert(1_000_000), convert(0.12), 240, convert(0), convert(7157.06),
				convert(0.01), BigDecimal.valueOf(0.012).setScale(3, RoundingMode.HALF_UP));
		assertEquals(BigDecimal.valueOf(12000).setScale(2), propertyBond.getLegalFees());
	}

	@Test
	public void bondCostsCalculatesAsExpected() {
		propertyBond = createPropertyBond(convert(1_000_000), convert(0.12), 240, convert(0), convert(7157.06),
				convert(0.01), BigDecimal.valueOf(0.012).setScale(3, RoundingMode.HALF_UP));
		assertEquals(BigDecimal.valueOf(10000).setScale(2), propertyBond.getBondCosts());
	}

	@Test
	public void interestAfterChangeInNominalRate() {
		propertyBond = createPropertyBond(convert(1_000_000), BigDecimal.valueOf(0.115).setScale(3), 240, convert(0),
				convert(7157.06), convert(0.01), BigDecimal.valueOf(0.012).setScale(3, RoundingMode.HALF_UP));
		propertyBond.useMinimumRepayment(1, 240);
		assertEquals(convert(9530.53), propertyBond.getInterestFor(6));
		propertyBond.setNominalRateFor(7, BigDecimal.valueOf(0.12).setScale(2));
		assertEquals(convert(9616.88), propertyBond.getInterestFor(33));
	}

	@Test
	public void repaymentAfterChangeInNominalRate() {
		propertyBond = createPropertyBond(convert(1_000_000), BigDecimal.valueOf(0.115).setScale(3), 240, convert(0),
				convert(7157.06), convert(0.01), BigDecimal.valueOf(0.012).setScale(3, RoundingMode.HALF_UP));
		propertyBond.useMinimumRepayment(1, 240);
		assertEquals(convert(10664.30), propertyBond.getRepaymentFor(6));
		propertyBond.setNominalRateFor(7, BigDecimal.valueOf(0.12).setScale(2));
		assertEquals(convert(11006.16), propertyBond.getRepaymentFor(33));
	}

	@Test
	public void ChangeInNominalRate() {
		propertyBond = createPropertyBond(convert(1_000_000), BigDecimal.valueOf(0.115).setScale(3), 240, convert(0),
				convert(7157.06), convert(0.01), BigDecimal.valueOf(0.012).setScale(3, RoundingMode.HALF_UP));
		propertyBond.useMinimumRepayment(1, 240);
		assertEquals(convert(9583.33), propertyBond.getInterestFor(1));
	}

	@Test
	public void AfterChangeInNominalRate() {
		propertyBond = createPropertyBond(convert(1_000_000), BigDecimal.valueOf(0.115).setScale(3), 240, convert(0),
				convert(7157.06), convert(0.01), BigDecimal.valueOf(0.012).setScale(3, RoundingMode.HALF_UP));
		propertyBond.useMinimumRepayment(1, 240);
		assertEquals(convert(10664.30), propertyBond.getRepaymentFor(1));
	}

	@Test
	public void closingAfterDeposit() {
		propertyBond = createPropertyBond(convert(1_000_000), BigDecimal.valueOf(0.115).setScale(3), 240, convert(0),
				convert(7157.06), convert(0.01), BigDecimal.valueOf(0.012).setScale(3, RoundingMode.HALF_UP));
		propertyBond.useMinimumRepayment(1, 240);
		propertyBond.addDepositFor(2, convert(1000));
		assertEquals(convert(997_837.29), propertyBond.getClosingBalanceFor(2));
	}

	@Test
	public void closingAfterWithdrawal() {
		propertyBond = createPropertyBond(convert(1_000_000), BigDecimal.valueOf(0.115).setScale(3), 240, convert(0),
				convert(7157.06), convert(0.01), BigDecimal.valueOf(0.012).setScale(3, RoundingMode.HALF_UP));
		propertyBond.useMinimumRepayment(1, 240);
		propertyBond.addWithdrawalFor(2, convert(500));
		assertEquals(convert(997_822.91), propertyBond.getClosingBalanceFor(2));
	}

	@Test
	public void closingAfterDepositAndWithdrawal() {
		propertyBond = createPropertyBond(convert(1_000_000), BigDecimal.valueOf(0.115).setScale(3), 240, convert(0),
				convert(7157.06), convert(0.01), BigDecimal.valueOf(0.012).setScale(3, RoundingMode.HALF_UP));
		propertyBond.useMinimumRepayment(1, 240);
		propertyBond.addDepositFor(2, convert(1000));
		propertyBond.addWithdrawalFor(2, convert(500));
		assertEquals(convert(997_832.50), propertyBond.getClosingBalanceFor(2));
	}

	@Test
	public void closingAfterMultipleDepositsAndWithdrawals() {
		propertyBond = createPropertyBond(convert(1_000_000), BigDecimal.valueOf(0.115).setScale(3), 240, convert(0),
				convert(7157.06), convert(0.01), BigDecimal.valueOf(0.012).setScale(3, RoundingMode.HALF_UP));
		propertyBond.useMinimumRepayment(1, 240);
		propertyBond.addDepositFor(2, convert(1000));
		propertyBond.addWithdrawalFor(2, convert(500));
		propertyBond.addDepositFor(2, convert(1000));
		propertyBond.addWithdrawalFor(2, convert(500));
		propertyBond.addDepositFor(2, convert(1000));
		propertyBond.addWithdrawalFor(2, convert(500));
		assertEquals(convert(997_842.08), propertyBond.getClosingBalanceFor(2));
	}

	@Test
	public void closingAfterBankingFees() {
		propertyBond = createPropertyBond(convert(1_000_000), BigDecimal.valueOf(0.115).setScale(3), 240, convert(0),
				convert(7157.06), convert(0.01), BigDecimal.valueOf(0.012).setScale(3, RoundingMode.HALF_UP));
		propertyBond.useMinimumRepayment(1, 240);
		propertyBond.addBankFeesFor(2, convert(100));
		assertEquals(convert(997_827.70), propertyBond.getClosingBalanceFor(2));
	}

}