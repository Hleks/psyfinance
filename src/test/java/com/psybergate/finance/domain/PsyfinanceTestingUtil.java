package com.psybergate.finance.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.math.BigDecimal;

import com.psybergate.finance.PsyfinanceException;
import com.psybergate.finance.domain.propertybond.PropertyBond;

public class PsyfinanceTestingUtil {

	public static void assertPsyfinanceExceptionThrown(Runnable methodToCheck, String message) {
		try {
			methodToCheck.run();
			fail(message);
		} catch ( PsyfinanceException expected ) {
			// Test passed
		}
	}

	public static void assertMonetaryAmountsEqual(double expected, BigDecimal actual) {
		assertEquals(BigDecimal.valueOf(expected).setScale(2), actual);
	}

	public static void assertMonthNumInvalidForMethod(String methodName, PropertyBond propertyBond, int pastMax) {
		PsyfinanceTestingUtil.assertPsyfinanceExceptionThrown(() -> invokeMethodWithArguments(methodName, propertyBond, -1),
				"Cannot be called with negative month number");
		PsyfinanceTestingUtil.assertPsyfinanceExceptionThrown(
				() -> invokeMethodWithArguments(methodName, propertyBond, pastMax + 1),
				"Cannot be called with month number greater than full term");
	}

	public static void invokeMethodWithArguments(String methodName, PropertyBond propertyBond, Object... args) {
		try {
			MethodHandle methodToTest = MethodHandles.lookup()
					.findVirtual(propertyBond.getClass(), methodName, MethodType.methodType(BigDecimal.class, int.class))
					.bindTo(propertyBond);
			methodToTest.invokeWithArguments(args);
		} catch ( Throwable e ) {
			throw new PsyfinanceException(e);
		}

	}

}
