package com.psybergate.finance.domain;

import static com.psybergate.finance.util.BigDecimalUtil.convert;
import static com.psybergate.finance.util.BigDecimalUtil.percent;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.psybergate.finance.PsyfinanceException;
import com.psybergate.finance.domain.interestbearinginstrument.InterestBearingMonth;
import com.psybergate.finance.domain.investment.Investment;
import com.psybergate.finance.domain.investment.Investment;

public class InvestmentTest {

	private Investment investment;

	@Before
	public void setup() {
		investment = new Investment(convert(1_000_000), convert(0.08), 60);
	}

	@Test
	public void construction() {
		assertEquals(convert(1_000_000), investment.getPrincipal());
		assertEquals(convert(0.08), investment.getNominalRate());
		assertEquals(60, investment.getTermInMonths());
		assertNotNull(investment.getMonths());
		assertEquals(60, investment.getMonths().size());
	}

	@Test
	public void ensureValidConstruction() {
		assertInvalidForConstruction(null, convert(0.08), 60, "Initial deposit cannot be null.");
		assertFalse(investment.getPrincipal().compareTo(BigDecimal.ZERO) < 0);

		assertInvalidForConstruction(convert(1_000_000), null, 60, "Nominal rate cannot be null");
		assertFalse(investment.getNominalRate().compareTo(BigDecimal.ZERO) < 0);

		assertFalse(investment.getTermInMonths() < 0);
	}

	private void assertInvalidForConstruction(BigDecimal openingBalance, BigDecimal nominalRate, int termInMonths,
			String message) {
		try {
			investment = new Investment(openingBalance, nominalRate, termInMonths);
			fail(message);
		} catch (PsyfinanceException expected) {
		}
	}

	@Test
	public void monthsNotNull() {
		for (InterestBearingMonth month : investment.getMonths()) {
			assertNotNull(month);
		}
	}

	@Test
	public void getPrincipalFor() {
		assertEquals(convert(1_357_505.66), investment.getPrincipalFor(47));
	}

	@Test
	public void interestForASpecificMonth() {
		assertEquals(convert(9050.04), investment.getInterestFor(47));
	}

	@Test
	public void closingForASpecificMonth() {
		assertEquals(convert(1_375_666.07), investment.getClosingBalanceFor(48));
	}

	@Test
	public void ClosingBalanceCorrectly() {
		assertEquals(convert(1_489_845.69), investment.getClosingBalance());
	}

	@Test
	public void cumalitiveInterest() {
		assertEquals(convert(489_845.69), investment.getCumalitiveInterest());
	}

	@Test
	public void getCumalitiveInterestUpTo() {
		assertEquals(convert(134_561.57), investment.getCumalitiveInterestUpTo(19));
	}

	@Test
	public void openingBalanceAndMonthlyContribution() {
		investment.setMonthlyContribution(convert(1000));
		assertEquals(convert(1_000_000), investment.getMonths().get(0).getOpening());
	}

	@Test
	public void closingBalanceAndMonthlyContribution() {
		investment.setMonthlyContribution(convert(1000));
		assertEquals(convert(1_007_673.33), investment.getClosingBalanceFor(1));
	}
	
	@Test
	public void correctClosingBalanceWithRegularBankFees() {
		investment = new Investment(convert(0), percent(22.4), convert(1000), convert(50), 60);
		assertEquals(convert(2960.59), investment.getClosingBalanceFor(3));
		assertEquals(convert(5027.56), investment.getClosingBalanceFor(5));
		assertEquals(convert(7172.42), investment.getClosingBalanceFor(7));
	}
}