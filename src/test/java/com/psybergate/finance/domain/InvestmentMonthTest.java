package com.psybergate.finance.domain;

import static com.psybergate.finance.util.BigDecimalUtil.convert;
import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.DoubleStream;

import org.junit.Before;
import org.junit.Test;

import com.psybergate.finance.domain.investment.InvestmentMonth;
import com.psybergate.finance.service.investment.InvestmentServiceImpl;

public class InvestmentMonthTest {

	private InvestmentMonth month;

	private InvestmentServiceImpl service = new InvestmentServiceImpl();

	@Before
	public void setup() {
		month = new InvestmentMonth(convert(1_000_000), convert(0.08));
	}

	private BigDecimal total(double... amounts) {
		return convert(DoubleStream.of(amounts).sum());
	}

	private BigDecimal randomBigDecimal(int max) {
		return convert(ThreadLocalRandom.current().nextDouble(System.currentTimeMillis(), System.currentTimeMillis() + max));
	}

//	@Test
//	public void getChangeToOpening() {
//		BigDecimal deposit1 = randomBigDecimal(500);
//		BigDecimal deposit2 = randomBigDecimal(99999);
//		BigDecimal deposit3 = randomBigDecimal(9);
//		BigDecimal withdrawal = randomBigDecimal(300);
//
//		service.deposit(month, deposit1, deposit2, deposit3);
//		service.withdraw(month, withdrawal);
//		BigDecimal changeTotal = total(deposit1.doubleValue(), deposit2.doubleValue(), deposit3.doubleValue(), -withdrawal.doubleValue());
//
//		assertEquals(changeTotal.setScale(2, RoundingMode.HALF_UP), month.getChangeToOpening());
//	}

	@Test
	public void getInterestGenerated() {
		assertEquals(convert(6_666.67), month.getInterest());
	}

	@Test
	public void getClosing() {
		assertEquals(convert(1_006_666.67), month.getClosing());
	}
}