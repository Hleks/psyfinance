package com.psybergate.finance.util;

import static com.psybergate.finance.util.BigDecimalUtil.*;
import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;

public class BigDecimalUtilTest {

	@Test
	public void addBigDecimals() {
		assertEquals(convert(128), add(convert(100), convert(12), convert(7), convert(9)));
	}
	
	@Test
	public void addDoubes() {
		assertEquals(convert(128), add(100, 12, 7, 9));
	}
	
	@Test 
	public void subtractBigDecimals() {
		assertEquals(convert(20), subtract(convert(2019), ZERO, convert(1980), convert(19)));
	}
	
	@Test 
	public void subtractDoubles() {
		assertEquals(convert(20), subtract(2019, 0, 1980, 19));
	}
	
	@Test
	public void multiplyBigDecimals() {
		assertEquals(convert(3600), multiply(convert(12), convert(300), 2));
	}

	@Test
	public void multiplyDoubles() {
		assertEquals(convert(3600), multiply(12, 300, 2));
	}

	@Test
	public void divideBigDecimals() {
		assertEquals(convert(33.33), divide(convert(100), convert(3), 2));
	}

	@Test
	public void divideDoubles() {
		assertEquals(convert(33.33), divide(100, 3, 2));
	}

	@Test
	public void percentTest() {
		assertEquals(BigDecimal.valueOf(0.080).setScale(3), percent(8));
	}
}