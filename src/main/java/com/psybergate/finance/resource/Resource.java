package com.psybergate.finance.resource;

import java.util.Set;

public interface Resource<T> {

	T find(Object primaryKey);

	Set<T> findAll();

	void save(T entity);

	T update(T entity);

	void delete(T entity);

	void deleteById(Object primaryKey);

}
