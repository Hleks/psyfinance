package com.psybergate.finance.resource;

import javax.enterprise.context.Dependent;
import javax.persistence.Query;

import com.psybergate.finance.domain.propertybond.PropertyBond;

@Dependent
public class PropertyBondResource extends AbstractResource<PropertyBond> {

	public PropertyBondResource() {
		super(PropertyBond.class);
	}

	@Override
	public void save(PropertyBond propertybond) {
		if (propertybond.getId() == null) {
			super.save(propertybond);
		}
		else {
			update(propertybond);
		}
	}

	@Override
	public PropertyBond find(Object propertyBondID) {
		return entityManager.find(PropertyBond.class, propertyBondID);
	}

	public PropertyBond findByUnique(String propertyBondName) {
		try {
			Query query = entityManager.createQuery("Select p from propertybond p WHERE p.name= :name");
			query.setParameter("name", propertyBondName);
			return (PropertyBond) query.getSingleResult();
		} catch ( IllegalArgumentException e ) {
			return null;
		}
	}
}
