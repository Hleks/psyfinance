package com.psybergate.finance.resource;

import javax.enterprise.context.Dependent;
import javax.persistence.Query;

import com.psybergate.finance.domain.investment.Investment;

@Dependent
public class InvestmentResource extends AbstractResource<Investment> {

	public InvestmentResource() {
		super(Investment.class);
	}

	@Override
	public void save(Investment investment) {
		if (investment.getId() == null) {
			super.save(investment);
		} else {
			update(investment);
		}
	}

	@Override
	public Investment find(Object investmentID) {
		return entityManager.find(Investment.class, investmentID);
	}

	public Investment findByUnique(String investmentName) {
		try {
			Query query = entityManager.createQuery("Select I from investment I WHERE c.name= :name");
			query.setParameter("name", investmentName);
			return (Investment) query.getSingleResult();
		} catch (IllegalArgumentException e) {
			return null;
		}
	}
}
