package com.psybergate.finance.resource;

import javax.enterprise.context.Dependent;

import com.psybergate.finance.domain.event.InvestmentEvent;

@Dependent
public class InvestmentEventResource extends AbstractResource<InvestmentEvent> {

	public InvestmentEventResource(Class<InvestmentEvent> entityClass) {
		super(InvestmentEvent.class);
	}

}
