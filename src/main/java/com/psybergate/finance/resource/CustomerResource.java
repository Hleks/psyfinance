package com.psybergate.finance.resource;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.Dependent;
import javax.persistence.Query;

import com.psybergate.finance.domain.Customer;

@Dependent
public class CustomerResource extends AbstractResource<Customer> {

	public CustomerResource() {
		super(Customer.class);
	}

	@Override
	public void save(Customer customer) {
		if (customer.getId() != null) {
			entityManager.merge(customer);
		} else {
			entityManager.persist(customer);
		}
	}

	@Override
	public Set<Customer> findAll() {
		List<Customer> entries = entityManager.createQuery("SELECT x FROM Customer x", Customer.class).getResultList();
		return new HashSet<Customer>(entries);
	}

	public Customer findByUnique(String email) {
		try {
			Query query = entityManager.createQuery("Select c from Customer c WHERE c.email= :email");
			query.setParameter("email", email);
			return (Customer) query.getSingleResult();
		} catch ( Exception e ) {
			return null;
		}
	}

}
