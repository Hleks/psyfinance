package com.psybergate.finance.resource;

import java.util.Set;

import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Dependent
public abstract class AbstractResource<T> implements Resource<T> {

	private Class<T> entityClass;

	@PersistenceContext(unitName = "psyfinance")
	protected EntityManager entityManager;

	public AbstractResource(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	@Override
	public void save(T entity) {
		entityManager.persist(entity);
	}

	@Override
	public T update(T entity) {
		return entityManager.merge(entity);
	}

	@Override
	public T find(Object primaryKey) {
		return entityManager.find(entityClass, primaryKey);
	}

	@Override
	public Set<T> findAll() {
		return null;
	}

	@Override
	public void delete(T entity) {
	}

	@Override
	public void deleteById(Object primaryKey) {
	}
}
