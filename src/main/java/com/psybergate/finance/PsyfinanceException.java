package com.psybergate.finance;

public class PsyfinanceException extends RuntimeException {

	private static final long serialVersionUID = -6259719329667274922L;

	public PsyfinanceException() {
		super();
	}

	public PsyfinanceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PsyfinanceException(String message, Throwable cause) {
		super(message, cause);
	}

	public PsyfinanceException(String message) {
		super(message);
	}

	public PsyfinanceException(Throwable cause) {
		super(cause);
	}

}
