package com.psybergate.finance.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class FinancialMath {

	private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;

	private static final BigDecimal ONE = BigDecimal.ONE;

	// ------------------Basic Time Value of Money----------------------

	public static BigDecimal futureValue(BigDecimal presentValue, BigDecimal periodicRate, int compoundingPeriods) {
		return periodicRate.add(ONE).pow(compoundingPeriods).multiply(presentValue);
	}

	public static BigDecimal futureValue(BigDecimal presentValue, BigDecimal nominalRate, int compoundingPeriodsPerYear,
			int years) {
		if (compoundingPeriodsPerYear < 0) throw new IllegalArgumentException();
		int compoundingPeriods = compoundingPeriodsPerYear * years;
		BigDecimal periodRate = nominalRate.divide(BigDecimal.valueOf(compoundingPeriodsPerYear), 2, ROUNDING_MODE);
		return futureValue(presentValue, periodRate, compoundingPeriods);
	}

	// ----------------------Compound Interest----------------------

	public static BigDecimal interestForMonth(BigDecimal principal, BigDecimal nominalRate) {
		BigDecimal monthlyInterestRate = nominalRate.divide(BigDecimal.valueOf(12), 20, RoundingMode.HALF_UP);
		return principal.multiply(monthlyInterestRate).setScale(2, RoundingMode.HALF_UP);
	}

	public static BigDecimal compoundInterest(BigDecimal principal, BigDecimal periodicRate, int compoundingPeriods) {
		return futureValue(principal, periodicRate, compoundingPeriods).subtract(principal);
	}

	public static BigDecimal compoundInterest(BigDecimal principal, BigDecimal nominalRate, int compoundingPeriodsPerYear,
			int years) {
		return futureValue(principal, nominalRate, compoundingPeriodsPerYear, years).subtract(principal);
	}

	public static BigDecimal annuityDuePayment(BigDecimal principal, BigDecimal nominalRate, int termInMonths, int monthNum) {
		return annuityDuePayment(principal, nominalRate, termInMonths - monthNum +1);
	}

	public static BigDecimal annuityDuePayment(BigDecimal principal, BigDecimal nominalRate, int compoundingPeriods) {
		BigDecimal one = BigDecimal.ONE;
		BigDecimal i = nominalRate.divide(BigDecimal.valueOf(12), 20, RoundingMode.HALF_UP);
		BigDecimal negPower = one.add(i).pow(compoundingPeriods);
		BigDecimal power = one.divide(negPower, 10, RoundingMode.HALF_UP);
		BigDecimal denominator = one.subtract(power);
		BigDecimal factor = i.divide(denominator, 20, RoundingMode.HALF_UP);
		return factor.multiply(principal).setScale(2, RoundingMode.HALF_UP);
	}
}
