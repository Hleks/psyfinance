package com.psybergate.finance.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.DoubleStream;

public class BigDecimalUtil {

	public static final BigDecimal ZERO = BigDecimal.ZERO;

	public static BigDecimal convert(double amount) {
		return BigDecimal.valueOf(amount).setScale(2, RoundingMode.HALF_UP);
	}

	public static BigDecimal add(BigDecimal... bigDecimals) {
		return sumList(Arrays.asList(bigDecimals));
	}

	public static BigDecimal add(double... numbers) {
		List<BigDecimal> bigDecimals = new ArrayList<>();
		DoubleStream.of(numbers).forEach(n -> bigDecimals.add(convert(n)));
		return add(bigDecimals.toArray(new BigDecimal[0]));
	}

	public static BigDecimal subtract(BigDecimal... bigDecimals) {
		BigDecimal difference = bigDecimals[0];

		
		for (int k = 1; k < bigDecimals.length; k++) {
			
			if (difference.compareTo(ZERO) < 0) {
				difference = difference.add(bigDecimals[k]);
			}
			else if (difference.compareTo(bigDecimals[k]) < 0) {
				difference = bigDecimals[k].subtract(difference).negate();
			}
			else {
				difference= difference.subtract(bigDecimals[k]);
			}
		}
		

		return difference;
	}

	public static BigDecimal subtract(double... numbers) {
		List<BigDecimal> bigDecimals = new ArrayList<>();
		DoubleStream.of(numbers).forEach(n -> bigDecimals.add(convert(n)));
		return subtract(bigDecimals.toArray(new BigDecimal[0]));
	}

	public static BigDecimal multiply(BigDecimal a, BigDecimal b, int scale) {
		return a.multiply(b).setScale(scale, RoundingMode.HALF_UP);
	}

	public static BigDecimal multiply(double a, double b, int scale) {
		return multiply(convert(a), convert(b), scale);
	}

	public static BigDecimal divide(BigDecimal a, BigDecimal b, int scale) {
		return a.divide(b, scale, RoundingMode.HALF_UP);
	}

	public static BigDecimal divide(double a, double b, int scale) {
		return divide(convert(a), convert(b), scale);
	}

	public static BigDecimal percent(double amount) {
		return divide(amount, 100, 3);
	}

	public static BigDecimal sumList(List<BigDecimal> amounts) {
		BigDecimal total = ZERO;
		for (BigDecimal amount : amounts) {
			total = total.add(amount);
		}
		return total;
	}

	public static BigDecimal percentageToDecimal(BigDecimal percentage) {
		return percentage.divide(BigDecimal.valueOf(100), 9, RoundingMode.HALF_UP).setScale(3, RoundingMode.HALF_EVEN);
	}

	public static BigDecimal decimalToPercentage(BigDecimal decimal) {
		if (decimal == null) return BigDecimal.ZERO;
		return decimal.multiply(BigDecimal.valueOf(100));
	}
}