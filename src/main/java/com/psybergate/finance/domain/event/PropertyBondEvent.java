package com.psybergate.finance.domain.event;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.psybergate.finance.domain.propertybond.PropertyBond;

@Entity
@Table(name = "property_bond_event")
public class PropertyBondEvent extends Event {

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "property_bond_id")
	private PropertyBond propertyBond;

	@Column
	private int endMonth = super.getMonthNum();

	public PropertyBondEvent() {
		super();
	}

	public PropertyBondEvent(int monthNum, BigDecimal amount, EventType typeOfChange) {
		super(monthNum, amount, typeOfChange);
	}

	public PropertyBondEvent(int monthNum, int endMonth, BigDecimal amount, EventType typeOfChange) {
		super(monthNum, amount, typeOfChange);
		this.endMonth = endMonth;
	}
 	
	
	public boolean affectsMonth(int month) {
		return month>=getMonthNum() && month<=endMonth;
	}

	public PropertyBond getPropertyBond() {
		return propertyBond;
	}

	public void setPropertyBond(PropertyBond propertyBond) {
		this.propertyBond = propertyBond;
	}
}
