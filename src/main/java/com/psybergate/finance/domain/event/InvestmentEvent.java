package com.psybergate.finance.domain.event;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.psybergate.finance.domain.investment.Investment;

@Entity
@Table(name = "investment_event")
public class InvestmentEvent extends Event {

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "investment_id")
	private Investment investment;

	public InvestmentEvent() {
		super();
	}

	public InvestmentEvent(int monthNum, BigDecimal amount, EventType typeOfChange) {
		super(monthNum, amount, typeOfChange);
	}
	
	public Investment getInvestment() {
		return investment;
	}
	
	public void setInvestment(Investment investment) {
		this.investment = investment;
	}
}
