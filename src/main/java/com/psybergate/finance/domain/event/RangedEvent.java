package com.psybergate.finance.domain.event;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ranged_event")
public class RangedEvent extends PropertyBondEvent {

	@Column
	private int endMonth;

	public RangedEvent(int monthNum, BigDecimal amount, EventType typeOfChange) {
		super(monthNum, amount, typeOfChange);
	}

	
	public int getEndMonth() {
		return endMonth;
	}

	public void setEndMonth(int endMonth) {
		this.endMonth = endMonth;
	}
}
