package com.psybergate.finance.domain.event;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Event {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column
	private int monthNum;

	@Column(precision = 19, scale = 3)
	private BigDecimal amount = BigDecimal.ZERO;

	@Column
	@Enumerated(EnumType.STRING)
	private EventType typeOfChange;

	public Event() {
	}

	public Event(int monthNum, BigDecimal amount, EventType typeOfChange) {
		super();
		this.monthNum = monthNum;
		this.amount = amount;
		this.typeOfChange = typeOfChange;
	}

	public Long getID() {
		return id;
	}
	
	public boolean isOfType(EventType type) {
		return typeOfChange.equals(type);
	}

	public EventType getTypeOfChange() {
		return typeOfChange;
	}
	
	public void setTypeOfChange(EventType typeOfChange) {
		this.typeOfChange = typeOfChange;
	}

	public void setEventID(Long eventID) {
		this.id = eventID;
	}

	public int getMonthNum() {
		return monthNum;
	}

	public void setMonthNum(int monthNum) {
		this.monthNum = monthNum;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Event other = (Event) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public boolean affectsMonth(int month) {
		return monthNum==month;
	}
}