package com.psybergate.finance.domain.event;

public enum EventType {
	DEPOSIT,
	WITHDRAWAL,
	RATE_CHANGE,
	BANK_FEE_CHANGE,
	MONTHLY_COST_CHANGE,
	MINIMUM_REPAYMENT;
}
