package com.psybergate.finance.domain.propertybond;

import static com.psybergate.finance.util.BigDecimalUtil.ZERO;
import static com.psybergate.finance.util.BigDecimalUtil.convert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.Dependent;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.psybergate.finance.PsyfinanceException;
import com.psybergate.finance.domain.event.EventType;
import com.psybergate.finance.domain.event.PropertyBondEvent;
import com.psybergate.finance.domain.interestbearinginstrument.InterestBearingInstrument;
import com.psybergate.finance.domain.interestbearinginstrument.InterestBearingMonth;
import com.psybergate.finance.domain.propertybond.transferduty.TransferDutyRates;
import com.psybergate.finance.domain.propertybond.transferduty.TransferDutyRates2019;

@Entity
@Table(name = "propertyBond")
@Dependent
public class PropertyBond extends InterestBearingInstrument {

	@Transient
	private TransferDutyRates transferDutyRates = new TransferDutyRates2019();

	@Column
	private BigDecimal purchasePrice;

	@Column(precision = 19, scale = 3)
	private BigDecimal initialDepositRate;

	@Column(precision = 19, scale = 3)
	private BigDecimal bondCostsRate;

	@Column(precision = 19, scale = 3)
	private BigDecimal legalFeesRate;

	@Column
	private BigDecimal monthlyRepayment;

	@Column(precision = 19, scale = 3)
	private BigDecimal bankFees = ZERO;

	@Transient
	private int finalPaymentMonth = -1;

	@OneToMany(mappedBy = "propertyBond", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<PropertyBondEvent> events = new ArrayList<>();

	public PropertyBond() {
		this.purchasePrice = ZERO;
		this.initialDepositRate = ZERO;
		this.bondCostsRate = ZERO;
		this.legalFeesRate = ZERO;
		this.monthlyRepayment = ZERO;
	}

	public PropertyBond(PropertyBond other) {
		this(other.nominalRate, other.purchasePrice, other.initialDepositRate, other.bondCostsRate, other.legalFeesRate,
				other.monthlyRepayment, other.termInMonths);
		this.bankFees = other.bankFees;
		this.events = other.events;
	}

	public PropertyBond(BigDecimal nominalRate, BigDecimal purchasePrice, BigDecimal initialDeposit, BigDecimal bondCosts,
			BigDecimal legalFees, BigDecimal monthlyRepayment, int termInMonths) {
		super(purchasePrice.subtract(initialDeposit), nominalRate, ZERO, termInMonths);
		validateForCreation(nominalRate, purchasePrice, initialDeposit, bondCosts, legalFees, monthlyRepayment,
				termInMonths);
		this.purchasePrice = purchasePrice.setScale(2);
		this.initialDepositRate = initialDeposit.setScale(3);
		this.bondCostsRate = bondCosts.setScale(3);
		this.legalFeesRate = legalFees.setScale(3);
		this.monthlyRepayment = monthlyRepayment.setScale(2);
		generateAllMonths(this.nominalRate, this.purchasePrice, this.monthlyRepayment, this.termInMonths);
	}

	public PropertyBond(double nominalRate, double purchasePrice, double initialDeposit, double bondCostRate,
			double legalFeesRate, double monthlyRepayment, int termInMonth) {
		this(BigDecimal.valueOf(nominalRate).setScale(3), convert(purchasePrice), convert(initialDeposit),
				BigDecimal.valueOf(bondCostRate).setScale(3), BigDecimal.valueOf(legalFeesRate).setScale(3),
				convert(monthlyRepayment), termInMonth);
	}

	public PropertyBond(double nominalRate, double purchasePrice, double initialDeposit, double bondCostRate,
			double legalFeesRate, int termInMonth) {
		this(BigDecimal.valueOf(nominalRate).setScale(3), convert(purchasePrice), convert(initialDeposit),
				BigDecimal.valueOf(bondCostRate).setScale(3), BigDecimal.valueOf(legalFeesRate).setScale(3), null, termInMonth);
	}

	private void validateForCreation(BigDecimal nominalRate, BigDecimal purchasePrice, BigDecimal initialDeposit,
			BigDecimal bondCosts, BigDecimal legalFees, BigDecimal monthlyRepayment, int termInMonths)
			throws PsyfinanceException {
		if (termInMonths < 0) throw new PsyfinanceException("Term in months must be greater than 0.");
		if (purchasePrice.compareTo(ZERO) < 0) throw new PsyfinanceException("Purchase price < 0.");
		if (nominalRate.compareTo(ZERO) < 0) throw new PsyfinanceException("Nominal rate < 0.");
	}

	@Override
	public void updateMonths() {
		finalPaymentMonth = -1;
		generateAllMonths(nominalRate, purchasePrice, monthlyRepayment, termInMonths);
	}

	private void generateAllMonths(BigDecimal nominalRate, BigDecimal purchasePrice, BigDecimal monthlyRepayment,
			int termInMonths) {
		months = new ArrayList<>();
		BigDecimal opening = getPrincipal();
		BigDecimal repayment = monthlyRepayment;
		BigDecimal bankFees = this.bankFees;

		for (int i = 1; i <= termInMonths; i++) {
			boolean useRequiredMinimumRepayment = false;

			PropertyBondMonth month = getNextPropertyBondMonth(opening, nominalRate, repayment, bankFees, i);

			for (PropertyBondEvent event : events) {
				BigDecimal amount = event.getAmount();

				if (event.affectsMonth(i)) {
					if (event.isOfType(EventType.RATE_CHANGE)) {
						nominalRate = amount;
					}
					else if (event.isOfType(EventType.BANK_FEE_CHANGE)) {
						bankFees = amount;
					}
					else if (event.isOfType(EventType.DEPOSIT)) {
						month.addDeposit(amount);
					}
					else if (event.isOfType(EventType.WITHDRAWAL)) {
						month.addWithdrawal(amount);
					}
					else if (event.isOfType(EventType.MONTHLY_COST_CHANGE)) {
						repayment = amount;
					}
					else if (event.isOfType(EventType.MINIMUM_REPAYMENT)) {
						useRequiredMinimumRepayment = true;
					}
				}
			}

			List<BigDecimal> deposits = month.getDeposits();
			List<BigDecimal> withdrawals = month.getWithdrawals();

			month = getNextPropertyBondMonth(opening, nominalRate, repayment, bankFees, i);
			month.setUseCalculatedMinimumRepayment(useRequiredMinimumRepayment);
			month.setRepayment(repayment);
			month.setDeposits(deposits);
			month.setWithdrawals(withdrawals);

			months.add(month);
			opening = month.getClosing();

			if (opening.compareTo(ZERO) <= 0 && finalPaymentMonth < 0) {
				finalPaymentMonth = i;
			}

		}
	}

	private PropertyBondMonth getNextPropertyBondMonth(BigDecimal opening, BigDecimal nominalRate, BigDecimal repayment,
			BigDecimal bankFee, int monthNum) {

		PropertyBondMonth month = new PropertyBondMonth(opening, nominalRate, repayment, bankFee, termInMonths, monthNum);
		return month;
	}

	public BigDecimal getTotalBondCost() {
		BigDecimal total = ZERO;
		for (InterestBearingMonth month : months) {
			PropertyBondMonth propertyBondMonth = (PropertyBondMonth) month;
			total = total.add(propertyBondMonth.getRepayment());
		}
		return total;
	}

	private void addEvent(PropertyBondEvent event) {
		event.setPropertyBond(this);
		events.add(event);
		updateMonths();
	}

	private void addEvent(int monthNum, BigDecimal amount, EventType eventType) {
		addEvent(new PropertyBondEvent(monthNum, amount, eventType));
	}

	public void useMinimumRepayment(int startMonth, int endMonth) {
		addEvent(new PropertyBondEvent(startMonth, endMonth, ZERO, EventType.MINIMUM_REPAYMENT));
	}

	public void addMonthlyRepaymentFor(int repaymentMonth, BigDecimal repaymentFees) {
		addEvent(repaymentMonth, repaymentFees, EventType.MONTHLY_COST_CHANGE);
	}

	public void setNominalRateFor(int monthNum, BigDecimal newNominalRate) {
		addEvent(monthNum, newNominalRate, EventType.RATE_CHANGE);
	}

	@Override
	public void addWithdrawalFor(int monthNum, BigDecimal amount) {
		super.addWithdrawalFor(monthNum, amount);
		addEvent(monthNum, amount, EventType.WITHDRAWAL);
	}

	@Override
	public void addDepositFor(int monthNum, BigDecimal amount) {
		super.addDepositFor(monthNum, amount);
		addEvent(monthNum, amount, EventType.DEPOSIT);
	}

	public void addBankFeesFor(int monthNum, BigDecimal newBankFee) {
		addEvent(monthNum, newBankFee, EventType.BANK_FEE_CHANGE);
	}

	public BigDecimal getTotalCashRequiredUpfront() {
		return getBondCosts().add(getLegalFees()).add(getTransferTax());
	}

	@Override
	public BigDecimal getBankFees() {
		return bankFees;
	}

	@Override
	public void setBankFees(BigDecimal bankFees) {
		this.bankFees = bankFees;
	}

	@Override
	public BigDecimal getPrincipal() {
		return purchasePrice.subtract(getInitialDeposit());
	}

	public List<PropertyBondEvent> getEvents() {
		return events;
	}

	public void setEvents(List<PropertyBondEvent> events) {
		this.events = events;
	}

	public BigDecimal getLegalFeesPercentage() {
		return legalFeesRate;
	}

	public BigDecimal getBondCostsPercentage() {
		return bondCostsRate;
	}

	public void setBondCostsPercentage(BigDecimal bondCostsRate) {
		this.bondCostsRate = bondCostsRate;
	}

	public void setLegalFeesPercentage(BigDecimal legalFeesRate) {
		this.legalFeesRate = legalFeesRate;
	}

	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(BigDecimal amount) {
		this.purchasePrice = amount;
	}

	public BigDecimal getInitialDeposit() {
		return initialDepositRate.multiply(purchasePrice);
	}

	public void setInitialDepositRate(BigDecimal amount) {
		this.initialDepositRate = amount;
	}

	public BigDecimal getInitialDepositRate() {
		return initialDepositRate;
	}

	public BigDecimal getRepaymentFor(int monthNum) {
		validateMonthNum(monthNum);
		return ((PropertyBondMonth) months.get(monthNum - 1)).getRepayment();
	}

	public BigDecimal getBondCosts() {
		return bondCostsRate.multiply(purchasePrice).setScale(2);
	}

	public BigDecimal getTransferTax() {
		return transferDutyRates.getTransferTaxDue(purchasePrice);
	}

	public BigDecimal getOutstandingPrincipalFor(int monthNum) {
		return months.get(monthNum - 1).getOpening();
	}

	public BigDecimal getLegalFees() {
		return legalFeesRate.multiply(purchasePrice).setScale(2);
	}

	public void setLegalFees(BigDecimal legalFees) {
		this.legalFeesRate = legalFees;
	}

	public BigDecimal getMonthlyRepayment() {
		return monthlyRepayment;
	}

	public void setMonthlyRepayment(BigDecimal monthlyRepayment) {
		this.monthlyRepayment = monthlyRepayment;
	}

	public int getFinalPaymentMonth() {
		return finalPaymentMonth;
	}

	@Override
	protected void updateMonthsFrom(int monthNum, String property, BigDecimal number) {
		updateMonths();
	}

}