package com.psybergate.finance.domain.propertybond.transferduty;

import java.math.BigDecimal;

public interface TransferDutyRates {
	
	BigDecimal getTransferTaxDue(BigDecimal purchasePrice);

}
