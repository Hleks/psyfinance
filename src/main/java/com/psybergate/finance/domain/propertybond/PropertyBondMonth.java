package com.psybergate.finance.domain.propertybond;

import static com.psybergate.finance.util.BigDecimalUtil.ZERO;
import static com.psybergate.finance.util.BigDecimalUtil.sumList;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.Dependent;

import com.psybergate.finance.domain.interestbearinginstrument.InterestBearingMonth;

@Dependent
public class PropertyBondMonth implements InterestBearingMonth {

	private BigDecimal opening;

	private BigDecimal nominalRate;

	private List<BigDecimal> deposits = new ArrayList<>();

	private List<BigDecimal> withdrawals = new ArrayList<>();

	private BigDecimal repayment = BigDecimal.ZERO;

	private BigDecimal bankFees = BigDecimal.ZERO;

	private boolean useCalculatedMinimumRepayment = false;

	private int compoundingPeriods;

	public PropertyBondMonth() {
	}

	public PropertyBondMonth(BigDecimal opening, BigDecimal nominalRate, BigDecimal repayment, BigDecimal bankFees,
			int termInMonths, int monthNum) {
		this.opening = opening;
		this.nominalRate = nominalRate;
		this.repayment = repayment;
		this.bankFees = bankFees;
		this.compoundingPeriods = termInMonths - monthNum +1;
	}

	public PropertyBondMonth(BigDecimal opening, BigDecimal nominalRate, BigDecimal repayment, BigDecimal bankFees) {
		this(opening, nominalRate, repayment, bankFees, 0, 0);
	}

	public PropertyBondMonth(BigDecimal opening, BigDecimal nominalRate, BigDecimal repayment) {
		this(opening, nominalRate, repayment, ZERO);
	}

	@Override
	public BigDecimal getChangeToOpening() {
		return sumList(deposits).subtract(sumList(withdrawals));
	}

	private BigDecimal getPrincipal() {
		return opening.subtract(getChangeToOpening());
	}

	@Override
	public BigDecimal getInterest() {
		BigDecimal principal = getPrincipal();
		return principal.multiply(getMonthlyRate()).setScale(2, RoundingMode.HALF_DOWN);
	}

	private BigDecimal getMonthlyRate() {
		return nominalRate.divide(BigDecimal.valueOf(12), 10, RoundingMode.HALF_UP);
	}

	@Override
	public BigDecimal getChangeAfterInterest() {
		return getRepayment().subtract(bankFees);
	}

	@Override
	public BigDecimal getClosing() {
		return getPrincipal().add(getInterest()).subtract(getChangeAfterInterest()).setScale(2);
	}

	public BigDecimal getRepayment() {
		BigDecimal repaymentAmount = this.repayment;
		if (useCalculatedMinimumRepayment) {
			repaymentAmount = getMinimumRequiredRepayment().add(bankFees);
		}
		repaymentAmount = adjustPaymentIfBondCanBePayedOff(repaymentAmount);
		return repaymentAmount;
	}

	public BigDecimal getMinimumRequiredRepayment() {
		return requiredMonthlyRepayment(getPrincipal(), nominalRate, compoundingPeriods);
	}

	public BigDecimal requiredMonthlyRepayment(BigDecimal principal, BigDecimal nominalRate, int compoundingPeriods) {
		BigDecimal one = BigDecimal.ONE;
		BigDecimal i = nominalRate.divide(BigDecimal.valueOf(12), 20, RoundingMode.HALF_UP);
		BigDecimal negPower = one.add(i).pow(compoundingPeriods);
		BigDecimal power = one.divide(negPower, 10, RoundingMode.HALF_UP);
		BigDecimal denominator = one.subtract(power);
		BigDecimal factor = i.divide(denominator, 20, RoundingMode.HALF_UP);
		return factor.multiply(principal).setScale(2, RoundingMode.HALF_UP);
	}

	private BigDecimal getAmountDue() {
		return getPrincipal().add(getInterest()).add(getBankFees());
	}

	private BigDecimal adjustPaymentIfBondCanBePayedOff(BigDecimal repayment) {
		BigDecimal amountDue = getAmountDue();

		if (amountDue.compareTo(repayment) < 0) {
			repayment = amountDue;
		}
		return repayment;
	}

	public void addDeposit(BigDecimal amount) {
		deposits.add(amount);
	}

	public void addWithdrawal(BigDecimal amount) {
		withdrawals.add(amount);
	}

	public BigDecimal getTotalDepositAmount() {
		return sumList(deposits);
	}

	public BigDecimal getTotalWithdrawalAmount() {
		return sumList(withdrawals);
	}

	public BigDecimal getBankFees() {
		return bankFees;
	}

	public void setBankFees(BigDecimal amount) {
		this.bankFees = amount;
	}

	public List<BigDecimal> getDeposits() {
		return deposits;
	}

	public List<BigDecimal> getWithdrawals() {
		return withdrawals;
	}

	public void setDeposits(List<BigDecimal> deposits) {
		this.deposits = deposits;
	}

	public void setWithdrawals(List<BigDecimal> withdrawals) {
		this.withdrawals = withdrawals;
	}

	public boolean isUseCalculatedMinimumRepayment() {
		return useCalculatedMinimumRepayment;
	}

	public void setUseCalculatedMinimumRepayment(boolean useCalculatedMinimumRepayment) {
		this.useCalculatedMinimumRepayment = useCalculatedMinimumRepayment;
	}

	public void setNominalRate(BigDecimal nominalRate) {
		this.nominalRate = nominalRate.setScale(3);
	}

	public void setRepayment(BigDecimal repayment) {
		this.repayment = repayment;
	}

	public void setOpening(BigDecimal opening) {
		this.opening = opening;
	}

	@Override
	public BigDecimal getOpening() {
		return opening;
	}

	public BigDecimal getNominalRate() {
		return nominalRate;
	}

}
