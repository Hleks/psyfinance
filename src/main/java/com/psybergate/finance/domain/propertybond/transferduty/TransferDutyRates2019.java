package com.psybergate.finance.domain.propertybond.transferduty;

import java.math.BigDecimal;

import javax.enterprise.inject.Default;
import javax.faces.bean.ApplicationScoped;

@ApplicationScoped @Default
public class TransferDutyRates2019 implements TransferDutyRates {

	@Override
	public BigDecimal getTransferTaxDue(BigDecimal purchasePrice) {
		if (purchasePrice.compareTo(BigDecimal.valueOf(900_000).setScale(2)) <= 0) {
			return BigDecimal.valueOf(0).setScale(2);
		}
		else if (purchasePrice.compareTo(BigDecimal.valueOf(1_250_000)) <= 0) {
			return transferDutyAmount(purchasePrice, BigDecimal.valueOf(0.03), BigDecimal.valueOf(900_000), BigDecimal.ZERO);
		}
		else if (purchasePrice.compareTo(BigDecimal.valueOf(1_750_000)) <= 0) {
			return transferDutyAmount(purchasePrice, BigDecimal.valueOf(0.06), BigDecimal.valueOf(1250000),
					BigDecimal.valueOf(10_500));
		}
		else if (purchasePrice.compareTo(BigDecimal.valueOf(2_250_000)) <= 0) {
			return transferDutyAmount(purchasePrice, BigDecimal.valueOf(0.08), BigDecimal.valueOf(1_750_000),
					BigDecimal.valueOf(40_500));
		}
		else if (purchasePrice.compareTo(BigDecimal.valueOf(10_000_000)) <= 0) {
			return transferDutyAmount(purchasePrice, BigDecimal.valueOf(0.11), BigDecimal.valueOf(2_250_000),
					BigDecimal.valueOf(80_500));
		}

		return transferDutyAmount(purchasePrice, BigDecimal.valueOf(0.13), BigDecimal.valueOf(10_000_000),
				BigDecimal.valueOf(933_000));
	}

	private BigDecimal transferDutyAmount(BigDecimal purchasePrice, BigDecimal percentage, BigDecimal threshhold,
			BigDecimal flatRate) {
		return purchasePrice.subtract(threshhold).multiply(percentage).add(flatRate).setScale(2);
	}

}
