package com.psybergate.finance.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.psybergate.finance.domain.interestbearinginstrument.InterestBearingInstrument;
import com.psybergate.finance.domain.investment.Investment;
import com.psybergate.finance.domain.propertybond.PropertyBond;

@Entity
@Table(name = "customer")
public class Customer {

	@Column
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column
	private String fullName;

	@Column(unique = true)
	private String email;

	@OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
	private List<Investment> investments = new ArrayList<>();

	@OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
	private List<PropertyBond> propertyBonds = new ArrayList<>();

	public Customer() {
	}

	public Customer(String fullName) {
		this.fullName = fullName;
	}

	public Customer(String fullName, String emailAddress) {
		this.fullName = fullName;
		this.email = emailAddress;
	}

	public void addForecast(InterestBearingInstrument interestBearingInstrument) {
		if (interestBearingInstrument instanceof Investment) {
			investments.add((Investment) interestBearingInstrument);
		}
		else if (interestBearingInstrument instanceof PropertyBond) {
			propertyBonds.add((PropertyBond) interestBearingInstrument);
		}
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Investment> getInvestments() {
		return investments;
	}

	public void setInvestments(List<Investment> investments) {
		this.investments = investments;
	}

	public List<PropertyBond> getPropertyBonds() {
		return propertyBonds;
	}

	public void setPropertyBonds(List<PropertyBond> propertyBonds) {
		this.propertyBonds = propertyBonds;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String emailAddress) {
		this.email = emailAddress;
	}
	
	@Override
	public String toString() {
		return "Customer [id=" + id + ", fullName=" + fullName + ", investments=" + investments + ", propertyBonds="
				+ propertyBonds + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Customer other = (Customer) obj;
		if (id != other.id) return false;
		return true;
	}


}
