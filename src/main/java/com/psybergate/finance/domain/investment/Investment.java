package com.psybergate.finance.domain.investment;

import static com.psybergate.finance.util.BigDecimalUtil.ZERO;
import static com.psybergate.finance.util.BigDecimalUtil.convert;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.Dependent;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.psybergate.finance.domain.event.EventType;
import com.psybergate.finance.domain.event.InvestmentEvent;
import com.psybergate.finance.domain.interestbearinginstrument.InterestBearingInstrument;

@Entity
@Table(name = "investment")
@Dependent
public class Investment extends InterestBearingInstrument {

	@Column
	private BigDecimal monthlyContribution = ZERO;

	@OneToMany(mappedBy = "investment", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<InvestmentEvent> events = new ArrayList<>();

	public Investment() {
		super();
	}

	public Investment(BigDecimal principal, BigDecimal nominalRate, BigDecimal monthlyContribution, BigDecimal bankFees,
			int termInMonths) {
		super(principal, nominalRate, bankFees, termInMonths);
		this.monthlyContribution = monthlyContribution;
		generateAllMonths(principal, nominalRate, monthlyContribution, bankFees);
	}

	public Investment(BigDecimal principal, BigDecimal nominalRate, BigDecimal monthlyContribution, int termInMonths) {
		this(principal, nominalRate, monthlyContribution, ZERO, termInMonths);
	}

	public Investment(BigDecimal principal, BigDecimal nominalRate, int termInMonths) {
		this(principal, nominalRate, ZERO, termInMonths);
	}

	public Investment(Investment other) {
		this(other.principal, other.nominalRate, other.monthlyContribution, other.bankFees, other.termInMonths);
		this.setId(other.getId());
		this.events = other.events;
	}

	private void generateAllMonths(BigDecimal initialDeposit, BigDecimal nominalRate, BigDecimal monthlyContribution,
			BigDecimal bankFees) {
		BigDecimal opening = initialDeposit;
		BigDecimal interestRate = nominalRate;
		InvestmentMonth month = null;
		months.clear();


		for (int k = 1; k <= termInMonths; k++) {
			month = new InvestmentMonth(opening, interestRate, monthlyContribution);

			for (InvestmentEvent event : events) {
				BigDecimal amount = event.getAmount();

				if (event.affectsMonth(k)) {
					if (event.isOfType(EventType.RATE_CHANGE)) {
						interestRate = amount;
					} else if (event.isOfType(EventType.BANK_FEE_CHANGE)) {
						bankFees = amount;
					} else if (event.isOfType(EventType.MONTHLY_COST_CHANGE)) {
						monthlyContribution = amount;
					} else if (event.isOfType(EventType.DEPOSIT)) {
						month.addDeposit(amount);
					} else if (event.isOfType(EventType.WITHDRAWAL)) {
						month.addWithdrawal(amount);
					}
				}
			
			}
			
			month.setNominalRate(interestRate);
			month.setMonthlyContribution(monthlyContribution);
			month.setBankFees(bankFees);
			months.add(month);
			opening = month.getClosing();
		}
	}

	@Override
	public void updateMonths() {
		generateAllMonths(principal, nominalRate, monthlyContribution, bankFees);
	}


	public List<InvestmentEvent> getEvents() {
		return events;
	}
	
	public void addEvent(int monthNum, BigDecimal amount, EventType type) {
		InvestmentEvent investmentEvent = new InvestmentEvent(monthNum, amount, type);
		investmentEvent.setInvestment(this);
		events.add(investmentEvent);
		updateMonths();
	}

	public void withdraw(int monthNum, double amount) {
		addEvent(monthNum, convert(amount), EventType.WITHDRAWAL);
	}

	public void addMonthlyContributionFor(int monthNum, BigDecimal contributionAmount) {
		addEvent(monthNum, contributionAmount, EventType.MONTHLY_COST_CHANGE);
	}

	public BigDecimal getMonthlyContribution() {
		return monthlyContribution;
	}

	public void setMonthlyContribution(BigDecimal monthlyContribution) {
		this.monthlyContribution = monthlyContribution;
		updateMonths();
	}

	public void setPrincipal(BigDecimal principal) {
		this.principal = principal;
		updateMonths();
	}

	public BigDecimal getEffectiveRate() {
		return nominalRate.divide(convert(12), 9, RoundingMode.HALF_DOWN).add(convert(1)).pow(12).subtract(convert(1))
				.multiply(BigDecimal.valueOf(100)).setScale(2, RoundingMode.HALF_UP);
	}

	public void deposit(int monthNum, double amount) {
		addEvent(monthNum, convert(amount), EventType.DEPOSIT);
	}

	public void setNominalRateFor(int monthNum, BigDecimal nominalRate) {
		addEvent(monthNum, nominalRate, EventType.RATE_CHANGE);
	}

	public void addBankFeesFor(int monthNum, BigDecimal amount) {
		addEvent(monthNum, amount, EventType.BANK_FEE_CHANGE);
	}

	public void setEvents(List<InvestmentEvent> events) {
		this.events = events;
	}

	@Override
	public String toString() {
		return "Investment [id=" + getId() + ", principal=" + principal + "]";
	}

	@Override
	protected void updateMonthsFrom(int monthNum, String property, BigDecimal number) {
		updateMonths();
	}
}