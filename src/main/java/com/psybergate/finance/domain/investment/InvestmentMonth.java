package com.psybergate.finance.domain.investment;

import static com.psybergate.finance.util.BigDecimalUtil.ZERO;
import static com.psybergate.finance.util.BigDecimalUtil.convert;
import static com.psybergate.finance.util.BigDecimalUtil.sumList;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.Dependent;

import com.psybergate.finance.domain.interestbearinginstrument.InterestBearingMonth;

@Dependent
public class InvestmentMonth implements InterestBearingMonth {

	private BigDecimal openingBalance = BigDecimal.ZERO;

	private BigDecimal nominalRate = BigDecimal.ZERO;

	private BigDecimal monthlyContribution = BigDecimal.ZERO;

	private List<BigDecimal> deposits = new ArrayList<>();

	private List<BigDecimal> withdrawals = new ArrayList<>();

	private BigDecimal bankFees = BigDecimal.ZERO;

	public InvestmentMonth() {
	}

	public InvestmentMonth(BigDecimal openingBalance, BigDecimal nominalRate, BigDecimal monthlyContribution,
			BigDecimal deposit, BigDecimal withdrawal) {
		this.openingBalance = openingBalance;
		this.nominalRate = nominalRate;
		this.monthlyContribution = monthlyContribution;
		this.deposits.add(deposit);
		this.withdrawals.add(withdrawal);
	}

	public InvestmentMonth(BigDecimal openingBalance, BigDecimal nominalRate, BigDecimal monthlyContribution) {
		this(openingBalance, nominalRate, monthlyContribution, ZERO, ZERO);
	}

	public InvestmentMonth(BigDecimal openingBalance, BigDecimal nominalRate) {
		this(openingBalance, nominalRate, ZERO);
	}

	public void setOpeningBalance(BigDecimal openingBalance) {
		this.openingBalance = openingBalance;
	}

	@Override
	public BigDecimal getOpening() {
		return openingBalance;
	}

	@Override
	public BigDecimal getNominalRate() {
		return nominalRate;
	}

	public void addDeposit(BigDecimal amount) {
		deposits.add(amount);
	}

	public void addDeposit(double amount) {
		deposits.add(convert(amount));
	}

	@Override
	public void addWithdrawal(BigDecimal amount) {
		withdrawals.add(amount);
	}

	public void addWithdrawal(double amount) {
		withdrawals.add(convert(amount));
	}

	@Override
	public BigDecimal getChangeToOpening() {
		return sumList(deposits).subtract(sumList(withdrawals)).add(monthlyContribution);
	}
	
	private BigDecimal getPrincipal() {
		return openingBalance.add(sumList(deposits)).subtract(sumList(withdrawals)).add(monthlyContribution);
	}

	@Override
	public BigDecimal getInterest() {
		BigDecimal monthlyInterestRate = nominalRate.divide(convert(12), 20, RoundingMode.HALF_UP);
		return getPrincipal().multiply(monthlyInterestRate).setScale(2, RoundingMode.HALF_DOWN);
	}

	@Override
	public BigDecimal getChangeAfterInterest() {
		return bankFees;
	}

	public BigDecimal getMonthlyContribution() {
		return monthlyContribution;
	}

	public void setMonthlyContribution(BigDecimal monthlyContribution) {
		this.monthlyContribution = monthlyContribution;
	}

	@Override
	public BigDecimal getClosing() {
		return getPrincipal().add(getInterest()).subtract(getChangeAfterInterest());
	}

	public BigDecimal getTotalDepositAmount() {
		return sumList(deposits);
	}

	public BigDecimal getTotalWithdrawalAmount() {
		return sumList(withdrawals);
	}

	@Override
	public List<BigDecimal> getWithdrawals() {
		return this.withdrawals;
	}

	@Override
	public List<BigDecimal> getDeposits() {
		return this.deposits;
	}

	@Override
	public void setDeposits(List<BigDecimal> deposits) {
		this.deposits = deposits;
	}

	@Override
	public void setWithdrawals(List<BigDecimal> withdrawals) {
		this.withdrawals = withdrawals;
	}

	@Override
	public void setBankFees(BigDecimal amount) {
		this.bankFees = amount;
	}

	@Override
	public BigDecimal getBankFees() {
		return this.bankFees;
	}
	
	public void setNominalRate(BigDecimal nominalRate) {
		this.nominalRate = nominalRate;
	}

}