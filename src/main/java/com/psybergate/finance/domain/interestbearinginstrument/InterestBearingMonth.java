package com.psybergate.finance.domain.interestbearinginstrument;

import java.math.BigDecimal;
import java.util.List;

public interface InterestBearingMonth {

	BigDecimal getOpening();

	BigDecimal getChangeToOpening();

	BigDecimal getInterest();

	BigDecimal getChangeAfterInterest();

	BigDecimal getClosing();

	BigDecimal getNominalRate();

	void addWithdrawal(BigDecimal withdrawal);

	void addDeposit(BigDecimal deposit);

	List<BigDecimal> getWithdrawals();

	List<BigDecimal> getDeposits();

	void setDeposits(List<BigDecimal> deposits);

	void setWithdrawals(List<BigDecimal> withdrawals);

	void setBankFees(BigDecimal amount);

	BigDecimal getBankFees();

	BigDecimal getTotalWithdrawalAmount();

	BigDecimal getTotalDepositAmount();	
	
}
