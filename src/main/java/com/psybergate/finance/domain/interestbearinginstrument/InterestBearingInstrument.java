package com.psybergate.finance.domain.interestbearinginstrument;

import static com.psybergate.finance.util.BigDecimalUtil.ZERO;
import static com.psybergate.finance.util.BigDecimalUtil.sumList;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import com.github.adminfaces.template.exception.BusinessException;
import com.psybergate.finance.PsyfinanceException;
import com.psybergate.finance.domain.Customer;

@MappedSuperclass
public abstract class InterestBearingInstrument {

	@Column
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@Column
	private String name;
	
	@Column
	protected BigDecimal principal = ZERO;

	@Column(precision = 19, scale = 3)
	protected BigDecimal nominalRate = ZERO;

	@Column
	protected int termInMonths = 1;

	@Column
	protected BigDecimal bankFees = ZERO;

	@Transient
	protected List<InterestBearingMonth> months = new ArrayList<>();

	public InterestBearingInstrument() {
	}

	public InterestBearingInstrument(BigDecimal principal, BigDecimal nominalRate, BigDecimal bankFees,
			int termInMonths) {
		validateForCreation(principal, nominalRate, termInMonths);
		this.principal = principal;
		this.nominalRate = nominalRate;
		this.termInMonths = termInMonths;
		this.bankFees = bankFees;
	}

	private void validateForCreation(BigDecimal principal, BigDecimal nominalRate, int termInMonths)
			throws PsyfinanceException {
		assertInputNotNull(principal, nominalRate);
		if (termInMonths < 0) throw new PsyfinanceException();
		if (nominalRate.compareTo(ZERO) < 0) throw new PsyfinanceException();
	}

	private void assertInputNotNull(BigDecimal principal, BigDecimal nominalRate) throws PsyfinanceException {
		if (principal == null) throw new PsyfinanceException();
		if (nominalRate == null) throw new PsyfinanceException();
	}

	protected void validateMonthNum(int monthNum) {
		String message = String.format("Please enter a month between 1 and %s", termInMonths);
		if (monthNum < 0 || monthNum > termInMonths) throw new BusinessException("Invalid month requested", message , FacesMessage.SEVERITY_WARN);
	}

	public void setPrincipal(BigDecimal principal) {
		this.principal = principal;
	}

	public abstract void updateMonths();

	protected abstract void updateMonthsFrom(int monthNum, String property, BigDecimal number);

	public void addDepositFor(int monthNum, BigDecimal amount) {
		months.get(monthNum - 1).addDeposit(amount);
	}

	public void addWithdrawalFor(int monthNum, BigDecimal amount) {
		months.get(monthNum - 1).addWithdrawal(amount);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrincipal() {
		return principal;
	}

	public BigDecimal getPrincipalFor(int monthNum) {
		validateMonthNum(monthNum);
		return months.get(monthNum - 1).getOpening();
	}

	public void setNominalRate(BigDecimal nominalRate) {
		this.nominalRate = nominalRate;
	}

	public BigDecimal getNominalRate() {
		return nominalRate;
	}

	public List<InterestBearingMonth> getMonths() {
		return months;
	}

	public InterestBearingMonth getMonth(int monthNum) {
		return months.get(monthNum);
	}

	public void setTermInMonths(int termInMonths) {
		this.termInMonths = termInMonths;
	}

	public int getTermInMonths() {
		return termInMonths;
	}

	public BigDecimal getInterestFor(int monthNum) {
		validateMonthNum(monthNum);
		return months.get(monthNum - 1).getInterest();
	}

	public BigDecimal getCumalitiveInterest() {
		BigDecimal total = ZERO;

		for (InterestBearingMonth month : months) {
			total = total.add(month.getInterest());
		}

		return total;
	}

	public BigDecimal getCumalitiveInterestUpTo(int monthNum) {
		if (monthNum < 1) throw new PsyfinanceException(new IllegalArgumentException());

		BigDecimal total = ZERO;

		for (int k = 0; k < monthNum; k++) {
			total = total.add(months.get(k).getInterest());
		}

		return total;
	}

	public BigDecimal getNominalRateFor(int monthNum) {
		return months.get(monthNum - 1).getNominalRate();
	}

	public void setBankFees(BigDecimal amount) {
		this.bankFees = amount;
	}

	public BigDecimal getBankFees() {
		return bankFees;
	}

	public BigDecimal getTotalBankFees() {
		List<BigDecimal> bankFees = new ArrayList<>();
		months.forEach(m -> bankFees.add(m.getBankFees()));
		return sumList(bankFees);
	}

	public BigDecimal getClosingBalanceFor(int monthNum) {
		validateMonthNum(monthNum);
		return months.get(monthNum - 1).getClosing();
	}

	public BigDecimal getClosingBalance() {
		if (months.size() != 0) {
			return getClosingBalanceFor(months.size());
		}
		else {
			return ZERO;
		}
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		InterestBearingInstrument other = (InterestBearingInstrument) obj;
		if (id != other.id) return false;
		return true;
	}

	
	public Customer getCustomer() {
		return customer;
	}

	
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	
	public Long getId() {
		return id;
	}

	
	public void setId(Long id) {
		this.id = id;
	}

	public String getForecastSummary() {
		return this.name + " - Currently at: " + this.principal + ", at a rate of: " + this.nominalRate + "% over: " + this.termInMonths
				+ " months";

	}
	
}