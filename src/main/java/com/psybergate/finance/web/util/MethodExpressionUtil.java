package com.psybergate.finance.web.util;

import java.math.BigDecimal;

import javax.el.MethodExpression;
import javax.faces.context.FacesContext;

public class MethodExpressionUtil {
	
	public static MethodExpression createMethodExpression(String methodExpression, Class<?> expectedReturnType, Class<?>[] expectedParamTypes) {
		FacesContext context = FacesContext.getCurrentInstance();
		return context.getApplication().getExpressionFactory()
				.createMethodExpression(context.getELContext(), methodExpression, expectedReturnType, expectedParamTypes);
	}

	public static MethodExpression createEventDialogMethodExpression(String elName, String methodName) {
    FacesContext context = FacesContext.getCurrentInstance();
    Class<?> expectedReturnType = void.class;
		Class<?>[] expectedParamTypes = {int.class, BigDecimal.class};
		
		String methodExpression = String.format("#{%s.%s(eventDialog.month, eventDialog.amount)}", elName, methodName);
		return context.getApplication().getExpressionFactory()
            .createMethodExpression(context.getELContext(), methodExpression, expectedReturnType , expectedParamTypes );
	}

}
