package com.psybergate.finance.web;

import static com.psybergate.finance.util.BigDecimalUtil.ZERO;
import static com.psybergate.finance.util.BigDecimalUtil.decimalToPercentage;

import java.math.BigDecimal;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.psybergate.finance.domain.investment.Investment;

@ManagedBean(name = "investmentDialog")
@RequestScoped
public class InvestmentDialog {

	private BigDecimal initialDeposit = ZERO;
	
	private BigDecimal nominalRate  = ZERO;

	private BigDecimal monthlyContribution = ZERO;

	private BigDecimal bankFees = ZERO;
	
	private int termInMonths=1;
	
	
	public InvestmentDialog() {
		// TODO Auto-generated constructor stub
	}
	
	public void setInvestment(Investment investment) {
		
		if(investment!=null) {			
			initialDeposit = investment.getPrincipal();
			nominalRate = decimalToPercentage(investment.getNominalRate());
			monthlyContribution = investment.getMonthlyContribution();
			bankFees = investment.getBankFees();
			termInMonths = investment.getTermInMonths();
		}
		
	}

	
	public BigDecimal getInitialDeposit() {
		return initialDeposit;
	}

	
	public void setInitialDeposit(BigDecimal initialDeposit) {
		this.initialDeposit = initialDeposit;
	}

	
	public BigDecimal getNominalRate() {
		return nominalRate;
	}

	
	public void setNominalRate(BigDecimal nominalRate) {
		this.nominalRate = nominalRate;
	}

	
	public BigDecimal getMonthlyContribution() {
		return monthlyContribution;
	}

	
	public void setMonthlyContribution(BigDecimal monthlyContribution) {
		this.monthlyContribution = monthlyContribution;
	}

	
	public BigDecimal getBankFees() {
		return bankFees;
	}

	
	public void setBankFees(BigDecimal bankFees) {
		this.bankFees = bankFees;
	}

	
	public int getTermInMonths() {
		return termInMonths;
	}

	
	public void setTermInMonths(int termInMonths) {
		this.termInMonths = termInMonths;
	}


}
