package com.psybergate.finance.web.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Specializes;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.util.Faces;

import com.github.adminfaces.template.exception.BusinessException;
import com.github.adminfaces.template.session.AdminSession;
import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.service.customer.CustomerService;

@Named("logonMB")
@SessionScoped
@Specializes
public class CustomerSession extends AdminSession implements Serializable {

	@Inject
	private CustomerService customerService;

	private String customerLabel;

	private String customerName;

	private String email;

	private boolean remember;

	private static final long serialVersionUID = 1716481508812644760L;

	public void login() throws IOException {
		Customer customer = null;
		try {
			customer = customerService.findCustomer(email);
			customerName = customer.getFullName();
			customerLabel = customerName;
			Faces.getExternalContext().getFlash().setKeepMessages(true);
			Faces.redirect("index.xhtml");

		} catch ( Exception e ) {
			FacesMessage.Severity severity = FacesMessage.SEVERITY_WARN;
			throw new BusinessException("Email not found. Please register as a new customer", "Email not registered. Please register as a new customer", severity);
		}

	}
	
	public void register() throws IOException {
		Customer customer = null;
		try {
			customer = new Customer(customerName, email);
			customerService.addCustomer(customer);
			customerLabel = customerName;
			Faces.getExternalContext().getFlash().setKeepMessages(true);
			Faces.redirect("index.xhtml");
		} catch ( Exception e ) {
			throw new BusinessException("We are unable to register you at this time. ", "We are unable to register you at this time.");
		}

	}

	@Override
	public boolean isLoggedIn() {

		return customerLabel != null;
	}

	public Customer getCustomer() {
		return customerService.findCustomer(email);
	}

	public String getCurrentUser() {
		return customerLabel;
	}

	public void setCurrentUser(String currentUser) {
		this.customerLabel = currentUser;
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public boolean isRemember() {
		return remember;
	}

	public void setRemember(boolean remember) {
		this.remember = remember;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
