package com.psybergate.finance.web.controller.truecost;

import static com.psybergate.finance.util.BigDecimalUtil.percentageToDecimal;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.el.MethodExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.ChartDataSet;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.interestbearinginstrument.InterestBearingInstrument;
import com.psybergate.finance.domain.interestbearinginstrument.InterestBearingMonth;
import com.psybergate.finance.domain.investment.Investment;
import com.psybergate.finance.domain.investment.InvestmentMonth;
import com.psybergate.finance.domain.propertybond.PropertyBond;
import com.psybergate.finance.domain.propertybond.PropertyBondMonth;
import com.psybergate.finance.web.Colour;
import com.psybergate.finance.web.util.MethodExpressionUtil;

@ManagedBean(name = "propertyBondTrueCost")
@ViewScoped
public class PropertyBondTrueCostController extends AbstractTrueCostController {

	@Override
	protected Map<String, InterestBearingInstrument> getFromService() {
		instruments = new HashMap<>();
		Customer customer = customerSession.getCustomer();
		customer.getPropertyBonds().forEach(this::addPropertyBond);
		return instruments;
	}

	private InterestBearingInstrument addPropertyBond(PropertyBond propertyBond) {
		return instruments.put(propertyBond.getName(), propertyBond);
	}

	public void addPropertyBondToSelection(BigDecimal nominalRate, BigDecimal purchasePrice, BigDecimal initialDeposit,
			BigDecimal monthlyRepayment, BigDecimal bondCosts, BigDecimal legalFees, BigDecimal bankFees, int termInMonths) {
		BigDecimal nominalRateAsDecimal = percentageToDecimal(nominalRate);
		BigDecimal initialDepositAsDecimal = percentageToDecimal(initialDeposit);
		PropertyBond propertyBond = new PropertyBond(nominalRateAsDecimal, purchasePrice, initialDepositAsDecimal,
				percentageToDecimal(bondCosts), percentageToDecimal(legalFees), monthlyRepayment, termInMonths);
		addInstrumentToSelection(propertyBond);
	}
	
	public void updateSelectedInstrument(String selected) {
		ChartData data = new ChartData();
		InterestBearingInstrument instrument = null;

			instrument = instruments.get(selected);
			
			if(instrument != null) {
				
				if(savedInstruments.contains(selected))
				{
					investment = createInstrumentImpactedByEvents(instrument);
					lineChartImpactOfEvents = createLineChartDataSet(investment, selected + " invested", Colour.GREEN);					
				}else {
					investment = createInstrumentImpactedByEvents(instrument);
					lineChartImpactOfEvents = createLineChartDataSet(investment, selected + " invested", Colour.GREEN);
				}
				
				data.addChartDataSet(lineChartImpactOfEvents);

					List<String> labels = getLabels(instrument);
					data.setLabels(labels);			
					lineModel.setData(data);
		}
	}

	@Override
	public MethodExpression getExpression() {
		Class<?>[] expectedParamTypes = { BigDecimal.class, BigDecimal.class, BigDecimal.class, BigDecimal.class,BigDecimal.class, BigDecimal.class,
				BigDecimal.class, int.class };
		MethodExpression methodExpression = MethodExpressionUtil.createMethodExpression(
				"#{propertyBondTrueCost.addPropertyBondToSelection(propertyBondDialog.nominalRate, propertyBondDialog.purchasePrice, propertyBondDialog.initialDepositRate,"
						+ " propertyBondDialog.monthlyRepayment, propertyBondDialog.bondCostsRate, propertyBondDialog.legalFeesRate, propertyBondDialog.bankFees, propertyBondDialog.termInMonths)}",
				void.class, expectedParamTypes);
		return methodExpression;
	}

	@Override
	public InterestBearingInstrument createInstrumentImpactedByEvents(InterestBearingInstrument original) {
		PropertyBond propertyBond = (PropertyBond) original;
		
		BigDecimal principal = propertyBond.getTotalCashRequiredUpfront().add(propertyBond.getInitialDeposit());
		BigDecimal nominalRate = BigDecimal.valueOf(0.07);
		BigDecimal monthlyContribution = propertyBond.getMonthlyRepayment();
		BigDecimal bankFees = propertyBond.getBankFees();
		int termInMonths = propertyBond.getTermInMonths();
		
		Investment investment = new Investment(principal, nominalRate, monthlyContribution, bankFees, termInMonths);
		investment.updateMonths();
		
		List<InterestBearingMonth> months = propertyBond.getMonths();
		
		for(int i=1; i<= propertyBond.getTermInMonths(); i++) {
			BigDecimal totalDepositAmount = months.get(i-1).getTotalDepositAmount();
			investment.deposit(i, totalDepositAmount.doubleValue());
			
			BigDecimal totalWithdrawalAmount = months.get(i-1).getTotalWithdrawalAmount();
			investment.withdraw(i, totalWithdrawalAmount.doubleValue());
			
			BigDecimal repayment = ((PropertyBondMonth) months.get(i-1)).getRepayment();
			InterestBearingMonth currentMonth = investment.getMonths().get(i-1);
	    ((InvestmentMonth) currentMonth).setMonthlyContribution(repayment);
		}
		
		return this.investment = investment;
	}
	
	public void updateAfterEvent() {
		lineChartImpactOfEvents = createLineChartDataSet(investment, selected + " After Events", "rgb(227, 75, 57)");
		ChartData data = lineModel.getData();
		List<ChartDataSet> dataSets = data.getDataSet();
		dataSets.set(1, lineChartImpactOfEvents);
	}

	public void addRateChangeEvent(int rateChangeMonth, BigDecimal rateChange) {
		((PropertyBond) investment).setNominalRateFor(rateChangeMonth, percentageToDecimal(rateChange));
		updateAfterEvent();
	}

	public void addDepositEvent(int depositMonth, BigDecimal deposit) {
		((PropertyBond) investment).addDepositFor(depositMonth, deposit);
		updateAfterEvent();
	}

	public void addWithdrawalEvent(int withdrawalMonth, BigDecimal withdrawal) {
		((PropertyBond) investment).addWithdrawalFor(withdrawalMonth, withdrawal);
		updateAfterEvent();
	}

	public void addBankFeesEvent(int bankingFeesMonth, BigDecimal bankingFees) {
		((PropertyBond) investment).addBankFeesFor(bankingFeesMonth, bankingFees);
		updateAfterEvent();
	}

	public void addMonthlyRepaymentChangeEvent(int repaymentMonth, BigDecimal repaymentFees) {
		((PropertyBond) investment).addMonthlyRepaymentFor(repaymentMonth, repaymentFees);
		updateAfterEvent();
	}

	public void useMinimumRepayment(int startMonth, int endMonth) {
		((PropertyBond) investment).useMinimumRepayment(startMonth, endMonth);
		updateAfterEvent();
	}

	@Override
	protected String getType() {
		return "Property Bond";
	}

}
