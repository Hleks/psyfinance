package com.psybergate.finance.web.controller;

import static com.psybergate.finance.util.BigDecimalUtil.convert;
import static com.psybergate.finance.util.BigDecimalUtil.decimalToPercentage;
import static com.psybergate.finance.util.BigDecimalUtil.percentageToDecimal;
import static com.psybergate.finance.util.BigDecimalUtil.sumList;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.MethodExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.PrimeFaces;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.pie.PieChartDataSet;
import org.primefaces.model.charts.pie.PieChartModel;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.interestbearinginstrument.InterestBearingMonth;
import com.psybergate.finance.domain.propertybond.PropertyBond;
import com.psybergate.finance.service.customer.CustomerService;
import com.psybergate.finance.service.propertybond.PropertyBondService;
import com.psybergate.finance.web.util.MethodExpressionUtil;

@ManagedBean(name = "propertyBondController")
@ViewScoped
public class PropertyBondController {

	@EJB
	private PropertyBondService propertyBondService;

	@Inject
	private CustomerService customerService;

	@Inject
	private CustomerSession customerSession;

	protected PropertyBond propertyBond = new PropertyBond();

	private LineChartModel lineModel = new LineChartModel();

	@PostConstruct
	public void init() {
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

		String propertyBondId = params.get("id");

		if (propertyBondId != null) {
			propertyBond = propertyBondService.getPropertyBond(Long.valueOf(propertyBondId));
			update();
		}
		else {
			propertyBond = new PropertyBond();
			PrimeFaces current = PrimeFaces.current();
			current.executeScript("PF('new-forecast-dlg').show();");
		}
	}

	public void addPropertyBond() {
		Customer customer = customerSession.getCustomer();
		customerService.addForecast(customer, propertyBond);
	}

	public void addRateChangeEvent(int rateChangeMonth, BigDecimal rateChange) {
		propertyBond.setNominalRateFor(rateChangeMonth, percentageToDecimal(rateChange));
		update();
	}

	public void update() {
		propertyBond.updateMonths();
		updateGraph();
	}

	public void useMinimumRepayment(int startMonth, int endMonth) {
		propertyBond.useMinimumRepayment(startMonth, endMonth);
	}

	public void addDepositEvent(int depositMonth, BigDecimal deposit) {
		propertyBond.addDepositFor(depositMonth, deposit);
		update();
	}

	public void addWithdrawalEvent(int withdrawalMonth, BigDecimal withdrawal) {
		propertyBond.addWithdrawalFor(withdrawalMonth, withdrawal);
		update();
	}

	public void addBankFeesEvent(int bankingFeesMonth, BigDecimal bankingFees) {
		propertyBond.addBankFeesFor(bankingFeesMonth, bankingFees);
		update();
	}

	public void addMonthlyRepaymentChangeEvent(int repaymentMonth, BigDecimal repaymentFees) {
		propertyBond.addMonthlyRepaymentFor(repaymentMonth, repaymentFees);
		update();
	}

	public PieChartModel getUpfrontCashPieModel() {
		PieChartModel pieModel = new PieChartModel();

		pieModel = new PieChartModel();
		ChartData data = new ChartData();

		PieChartDataSet dataSet = new PieChartDataSet();
		List<Number> values = new ArrayList<>();
		values.add(propertyBond.getTransferTax());
		values.add(propertyBond.getLegalFees());
		values.add(propertyBond.getBondCosts());
		dataSet.setData(values);

		List<String> bgColors = new ArrayList<>();
		bgColors.add("rgb(0, 165, 90)");
		bgColors.add("rgb(0, 192, 239)");
		bgColors.add("rgb(243, 156, 18)");
		dataSet.setBackgroundColor(bgColors);

		data.addChartDataSet(dataSet);
		List<String> labels = new ArrayList<>();
		labels.add("Transfer Duty");
		labels.add("Legal Fees");
		labels.add("Bond Costs");
		data.setLabels(labels);

		pieModel.setData(data);

		return pieModel;
	}

	public PieChartModel getTotalBondCostPieModel() {
		PieChartModel pieModel = new PieChartModel();

		pieModel = new PieChartModel();
		ChartData data = new ChartData();

		List<BigDecimal> withdrawals = new ArrayList<>();
		propertyBond.getMonths().forEach(m -> withdrawals.add(m.getTotalWithdrawalAmount()));

		BigDecimal withdrawalsTotal = sumList(withdrawals);

		PieChartDataSet dataSet = new PieChartDataSet();
		List<Number> values = new ArrayList<>();
		values.add(propertyBond.getCumalitiveInterest());
		values.add(propertyBond.getTotalBankFees());
		values.add(propertyBond.getPrincipal());
		values.add(withdrawalsTotal);
		dataSet.setData(values);

		List<String> bgColors = new ArrayList<>();
		bgColors.add("rgb(221, 75, 57)");
		bgColors.add("rgb(0, 165, 90)");
		bgColors.add("rgb(0, 192, 239)");
		bgColors.add("rgb(96, 92, 168)");
		dataSet.setBackgroundColor(bgColors);

		data.addChartDataSet(dataSet);
		List<String> labels = new ArrayList<>();
		labels.add("Cumalitive Interest");
		labels.add("Bank Fees");
		labels.add("Principal");
		labels.add("Withdrawals");
		data.setLabels(labels);

		pieModel.setData(data);

		return pieModel;
	}

	public void updateGraph() {
		lineModel = new LineChartModel();
		LineChartSeries s = new LineChartSeries();
		s.setLabel("Forecast");
		List<InterestBearingMonth> months = new ArrayList<>();
		months.addAll(getMonths());

		int i = 0;
		for (InterestBearingMonth month : months) {
			if (i % 3 == 0) {
				s.set(i, month.getClosing());
			}
			i++;
		}

		lineModel.addSeries(s);
		lineModel.setLegendPosition("e");
		Axis y = lineModel.getAxis(AxisType.Y);
		y.setMin(0);
		SortedSet<BigDecimal> outstandingPrincipalAmounts = new TreeSet<BigDecimal>();
		List<InterestBearingMonth> propertyBondMonths = propertyBond.getMonths();

		Double maxValue = 0d;
		if (propertyBondMonths.size() > 0) {
			propertyBondMonths.forEach(m -> outstandingPrincipalAmounts.add(m.getOpening()));
			BigDecimal maxValueInSet = outstandingPrincipalAmounts.last();
			maxValue = maxValueInSet.multiply(convert(1.2)).doubleValue();
		}

		y.setMax(maxValue);
		y.setLabel("Principal");

		int tick;

		if (maxValue <= 500_000) {
			tick = 50_000;
		}
		else if (maxValue < 1_500_001) {
			tick = 150_000;
		}
		else if (maxValue < 2_000_001) {
			tick = 500_000;
		}
		else if (maxValue < 6_000_001) {
			tick = 750_000;
		}
		else {
			tick = 1_000_000;
		}

		y.setTickInterval(String.valueOf(tick));

		Axis x = lineModel.getAxis(AxisType.X);
		x.setMin(0);
		x.setMax(propertyBond.getTermInMonths());
		x.setTickInterval("12");
		x.setLabel("Month");
	}

	public LineChartModel getLineModel() {
		return lineModel;
	}

	public String getCalendarColour() {
		if (propertyBond.getFinalPaymentMonth() < 0) return "red";
		return "green";
	}

	public String getCalendarIcon() {
		if (propertyBond.getFinalPaymentMonth() < 0) return "calendar-times";
		return "calendar-check";
	}

	public String getFinalMonthTitle() {
		if (propertyBond.getFinalPaymentMonth() < 0) return "";
		return "Paid off in month";
	}

	public String getFinalMonthBody() {
		if (propertyBond.getFinalPaymentMonth() < 0) return "Not paid off.";
		return String.valueOf(propertyBond.getFinalPaymentMonth());
	}

	public String getName() {
		return propertyBond.getName();
	}

	public void setName(String name) {
		propertyBond.setName(name);
	}

	public BigDecimal getBankFees() {
		return propertyBond.getBankFees();
	}

	public void setBankFees(BigDecimal bankFees) {
		propertyBond.setBankFees(bankFees);
	}

	public int getTermInMonths() {
		return propertyBond.getTermInMonths();
	}

	public void setTermInMonths(int termInMonths) {
		propertyBond.setTermInMonths(termInMonths);
	}

	public BigDecimal getPrincipal() {
		return propertyBond.getPrincipal();
	}

	public BigDecimal getInitialDepositRate() {
		return decimalToPercentage(propertyBond.getInitialDepositRate());
	}

	public void setInitialDepositRate(BigDecimal initialDepositRate) {
		propertyBond.setInitialDepositRate(percentageToDecimal(initialDepositRate));
	}

	public BigDecimal getInitialDeposit() {
		return propertyBond.getInitialDeposit();
	}

	public BigDecimal getClosingBalance() {
		return propertyBond.getClosingBalance();
	}

	public BigDecimal getPurchasePrice() {
		return propertyBond.getPurchasePrice();
	}

	public void setPurchasePrice(BigDecimal purchasePrice) {
		propertyBond.setPurchasePrice(purchasePrice);
	}

	public BigDecimal getCumalitiveInterest() {
		return propertyBond.getCumalitiveInterest();
	}

	public BigDecimal getMonthlyRepayment() {
		return propertyBond.getMonthlyRepayment();
	}

	public void setMonthlyRepayment(BigDecimal repayment) {
		propertyBond.setMonthlyRepayment(repayment);
	}

	public BigDecimal getBondCosts() {
		return propertyBond.getBondCosts();
	}

	public BigDecimal getTotalBondCost() {
		return propertyBond.getTotalBondCost();
	}

	public BigDecimal getLegalFees() {
		return propertyBond.getLegalFees();
	}

	public BigDecimal getTransferTax() {
		return propertyBond.getTransferTax();
	}

	public BigDecimal getTotalCashRequiredUpfront() {
		return propertyBond.getTotalCashRequiredUpfront();
	}

	public BigDecimal getOutstandingPrincipalFor(int monthNum) {
		return propertyBond.getOutstandingPrincipalFor(monthNum);
	}

	public List<InterestBearingMonth> getMonths() {
		return propertyBond.getMonths();
	}

	public int getFinalPaymentMonth() {
		return propertyBond.getFinalPaymentMonth();
	}

	public BigDecimal getLegalFeesPercentage() {
		return decimalToPercentage(propertyBond.getLegalFeesPercentage());
	}

	public BigDecimal getBondCostsPercentage() {
		return decimalToPercentage(propertyBond.getBondCostsPercentage());
	}

	public void setBondCostsPercentage(BigDecimal bondCosts) {
		propertyBond.setBondCostsPercentage(percentageToDecimal(bondCosts));
	}

	public void setLegalFeesPercentage(BigDecimal legalFees) {
		propertyBond.setLegalFeesPercentage(percentageToDecimal(legalFees));
	}

	public BigDecimal getNominalRatePercentage() {
		return decimalToPercentage(propertyBond.getNominalRate());
	}

	public void setNominalRatePercentage(BigDecimal nominalRate) {
		propertyBond.setNominalRate(percentageToDecimal(nominalRate));
	}

	public String getPropertyBondTitle() {
		if (propertyBond.getId() != null) { return propertyBond.getName(); }
		return "New Property Bond";
	}

	public void addNewPropertyBond(BigDecimal nominalRate, BigDecimal purchasePrice, BigDecimal initialDeposit,
			BigDecimal bondCosts, BigDecimal legalFees, BigDecimal monthlyRepayment, BigDecimal bankFees, int termInMonths) {
		PropertyBond newPropertyBond = new PropertyBond(percentageToDecimal(nominalRate), purchasePrice,
				percentageToDecimal(initialDeposit), percentageToDecimal(bondCosts), percentageToDecimal(legalFees),
				monthlyRepayment, termInMonths);
		newPropertyBond.setBankFees(bankFees);
		propertyBond = newPropertyBond;
		update();
	}

	public void editPropertyBond(BigDecimal nominalRate, BigDecimal purchasePrice, BigDecimal initialDeposit,
			BigDecimal bondCosts, BigDecimal legalFees, BigDecimal monthlyRepayment, BigDecimal bankFees, int termInMonths) {
		propertyBond.setNominalRate(percentageToDecimal(nominalRate));
		propertyBond.setPurchasePrice(purchasePrice);
		propertyBond.setInitialDepositRate(percentageToDecimal(initialDeposit));
		propertyBond.setBondCostsPercentage(percentageToDecimal(bondCosts));
		propertyBond.setLegalFeesPercentage(percentageToDecimal(legalFees));
		propertyBond.setMonthlyRepayment(monthlyRepayment);
		propertyBond.setBankFees(bankFees);
		propertyBond.setTermInMonths(termInMonths);
		update();
	}

	public MethodExpression getEditForecast() {
		Class<?> expectedReturnType = void.class;
		Class<?>[] expectedParamTypes = { BigDecimal.class, BigDecimal.class, BigDecimal.class, BigDecimal.class,
				BigDecimal.class, BigDecimal.class, int.class };
		MethodExpression methodExpression = MethodExpressionUtil.createMethodExpression(
				"#{propertyBondController.editPropertyBond(propertyBondDialog.nominalRate, propertyBondDialog.purchasePrice, propertyBondDialog.initialDepositRate, "
						+ " propertyBondDialog.bondCostsRate, propertyBondDialog.legalFeesRate, propertyBondDialog.monthlyRepayment,  propertyBondDialog.bankFees,  propertyBondDialog.termInMonths)}",
				expectedReturnType, expectedParamTypes);
		return methodExpression;
	}

	public MethodExpression getNewForecast() {
		Class<?> expectedReturnType = void.class;
		Class<?>[] expectedParamTypes = { BigDecimal.class, BigDecimal.class, BigDecimal.class, BigDecimal.class,
				BigDecimal.class, BigDecimal.class, BigDecimal.class, int.class };
		MethodExpression methodExpression = MethodExpressionUtil.createMethodExpression(
				"#{propertyBondController.addNewPropertyBond(propertyBondDialog.nominalRate, propertyBondDialog.purchasePrice, propertyBondDialog.initialDepositRate, "
						+ " propertyBondDialog.bondCostsRate, propertyBondDialog.legalFeesRate, propertyBondDialog.monthlyRepayment, propertyBondDialog.bankFees, propertyBondDialog.termInMonths)}",
				expectedReturnType, expectedParamTypes);
		return methodExpression;
	}

	public MethodExpression getSaveForecast() {
		Class<?> expectedReturnType = void.class;
		Class<?>[] expectedParamTypes = {};
		MethodExpression methodExpression = MethodExpressionUtil.createMethodExpression("#{propertyBondController.update}",
				expectedReturnType, expectedParamTypes);
		return methodExpression;
	}

	public PropertyBond getInstrument() {
		return propertyBond;
	}

}