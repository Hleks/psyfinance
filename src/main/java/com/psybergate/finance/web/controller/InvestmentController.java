package com.psybergate.finance.web.controller;

import static com.psybergate.finance.util.BigDecimalUtil.convert;
import static com.psybergate.finance.util.BigDecimalUtil.percentageToDecimal;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.MethodExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.PrimeFaces;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.interestbearinginstrument.InterestBearingMonth;
import com.psybergate.finance.domain.investment.Investment;
import com.psybergate.finance.service.customer.CustomerService;
import com.psybergate.finance.service.investment.InvestmentService;
import com.psybergate.finance.web.util.MethodExpressionUtil;

@ManagedBean(name = "investmentController")
@ViewScoped
public class InvestmentController {

	@EJB
	private InvestmentService investmentService;

	@Inject
	private CustomerService customerService;

	@Inject
	private CustomerSession customerSession;

	private Investment investment= new Investment();

	private LineChartModel lineModel;

	public InvestmentController() {
	}

	@PostConstruct
	public void init() {
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

		String investmentId = params.get("id");

		if (investmentId != null) {
			investment = investmentService.getInvestment(Long.valueOf(investmentId));
			investment.updateMonths();
		} else {
			PrimeFaces current = PrimeFaces.current();
			current.executeScript("PF('new-forecast-dlg').show();");
		}
		
		update();
	}

	public void update() {
		Investment oldInvestment = investment;
		investment = new Investment(oldInvestment);
		investment.updateMonths();
		updateGraph();
	}

	public void addRateChangeEvent(int rateChangeMonth, BigDecimal rateChange) {
		investmentService.setNominalRateFor(investment, rateChangeMonth, percentageToDecimal(rateChange));
		updateGraph();
	}

	public void addDepositEvent(int depositMonth, BigDecimal deposit) {
		investmentService.deposit(investment, depositMonth, deposit.doubleValue());
		updateGraph();
	}

	public void addWithdrawalEvent(int withdrawalMonth, BigDecimal withdrawal) {
		investmentService.withdraw(investment, withdrawalMonth, withdrawal.doubleValue());
		updateGraph();
	}

	public void addMonthlyContributionChangeEvent(int contributionMonth, BigDecimal contributionAmount) {
		investment.addMonthlyContributionFor(contributionMonth, contributionAmount);
		updateGraph();
	}

	public void addInvestment() {
		Customer customer = customerSession.getCustomer();
		customerService.addForecast(customer, investment);
	}

	public Investment getInvestment() {
		return investment;
	}

	public void setName(String name) {
		investment.setName(name);
	}

	public String getName() {
		return investment.getName();
	}

	public void setPrincipal(BigDecimal principal) {
		investment.setPrincipal(principal);
	}

	public BigDecimal getPrincipal() {
		return investment.getPrincipal();
	}

	public void setNominalRate(BigDecimal nominalRate) {
		investment.setNominalRate(nominalRate);
	}

	public BigDecimal getNominalRate() {
		return investment.getNominalRate();
	}

	public BigDecimal getNominalRatePercentage() {
		BigDecimal nominalRate = investment.getNominalRate();
		if (nominalRate == null) return BigDecimal.ZERO;
		return investment.getNominalRate().multiply(BigDecimal.valueOf(100));
	}

	public void setNominalRatePercentage(BigDecimal nominalRate) {
		BigDecimal nominalRateAsDecimal = nominalRate.divide(BigDecimal.valueOf(100), 9, RoundingMode.HALF_UP).setScale(3,
				RoundingMode.HALF_EVEN);
		investment.setNominalRate(nominalRateAsDecimal);
	}

	public BigDecimal getEffectiveRate() {
		try {
			return investment.getEffectiveRate().multiply(convert(10));
		} catch ( NullPointerException e ) {
			return BigDecimal.ZERO;
		}
	}

	public void setTermInMonths(int termInMonths) {
		investment.setTermInMonths(termInMonths);
	}

	public int getTermInMonths() {
		return investment.getTermInMonths();
	}

	public List<InterestBearingMonth> getMonths() {
		return investment.getMonths();
	}

	public void setMonthlyContribution(BigDecimal amount) {
		investment.setMonthlyContribution(amount);
	}

	public BigDecimal getMonthlyContribution() {
		return investment.getMonthlyContribution();
	}

	public void setBankFees(BigDecimal amount) {
		investment.setBankFees(amount);
	}

	public BigDecimal getBankFees() {
		return investment.getBankFees();
	}

	public BigDecimal getCumalitiveInterest() {
		return investment.getCumalitiveInterest();
	}

	public BigDecimal getTotalInterest() {
		return investmentService.calculateTotalInterestEarned(investment);
	}

	public BigDecimal getClosingBalance() {
		return investmentService.calculateClosingBalance(investment);
	}

	public void addBankFeesEvent(int bankingFeesMonth, BigDecimal bankingFees) {
		investment.addBankFeesFor(bankingFeesMonth, bankingFees);
		updateGraph();
	}

	public BigDecimal getTotalBankFees() {
		return investment.getTotalBankFees();
	}

	public void updateGraph() {
		lineModel = new LineChartModel();
		LineChartSeries s = new LineChartSeries();
		String name = (investment.getName() != null) ? investment.getName() : "Investment";
		s.setLabel(name);
		List<InterestBearingMonth> months = new ArrayList<>();

		months.addAll(getMonths());

		int numOfMonths = investment.getTermInMonths() + 1;
		int tick;
		int pointToTake = 1;

		if (numOfMonths <= 60) {
			tick = 3;
		}
		if (numOfMonths < 120) {
			tick = 6;
		}
		else {
			tick = 12;
			pointToTake = 3;
		}

		int i = 1;
		for (InterestBearingMonth month : months) {
			if (i % pointToTake == 0) {
				s.set(i, month.getClosing());
			}
			i++;
		}

		lineModel.addSeries(s);
		lineModel.setLegendPosition("e");
		Axis y = lineModel.getAxis(AxisType.Y);
		y.setMin(investment.getPrincipal());
		SortedSet<BigDecimal> capitalAmounts = new TreeSet<BigDecimal>();
		investment.getMonths().forEach(m -> capitalAmounts.add(m.getOpening()));
		capitalAmounts.add(investment.getClosingBalance());
		BigDecimal maxValueInSet = capitalAmounts.last();
		Double maxValue = maxValueInSet.multiply(convert(1.1)).doubleValue();
		y.setMax(maxValue);
		y.setLabel("Closing Balance");

		Axis x = lineModel.getAxis(AxisType.X);
		x.setMin(0);
		x.setMax(months.size() - 1);
		x.setTickInterval(String.valueOf(tick));
		x.setLabel("Month");
	}

	public LineChartModel getLineModel() {
		return lineModel;
	}

	public String getInvestmentTitle() {
		if (investment.getId() != null) { return investment.getName(); }
		return "New Investment";
	}


	public void addInvestment(BigDecimal principal, BigDecimal nominalRate, BigDecimal monthlyContribution,
			BigDecimal bankFees, int termInMonths) {
		Investment newInvestment = new Investment(principal, percentageToDecimal(nominalRate), monthlyContribution,
				bankFees, termInMonths);
		investment = newInvestment;
		investment.updateMonths();
		update();
	}

	public void editInvestment(BigDecimal principal, BigDecimal nominalRate, BigDecimal monthlyContribution,
			BigDecimal bankFees, int termInMonths) {
		investment.setPrincipal(principal);
		investment.setNominalRate(percentageToDecimal(nominalRate));
		investment.setMonthlyContribution(monthlyContribution);
		investment.setBankFees(bankFees);
		investment.setTermInMonths(termInMonths);
		investment.updateMonths();
		update();
	}

	public MethodExpression getEditForecast() {
		Class<?> expectedReturnType = void.class;
		Class<?>[] expectedParamTypes = { BigDecimal.class, BigDecimal.class, BigDecimal.class, BigDecimal.class,
				int.class };
		MethodExpression methodExpression = MethodExpressionUtil.createMethodExpression(
				"#{investmentController.editInvestment( investmentDialog.initialDeposit, investmentDialog.nominalRate,"
						+ " investmentDialog.monthlyContribution, investmentDialog.bankFees, investmentDialog.termInMonths)}",
				expectedReturnType, expectedParamTypes);
		return methodExpression;
	}

	public MethodExpression getNewForecast() {
		Class<?> expectedReturnType = void.class;
		Class<?>[] expectedParamTypes ={ BigDecimal.class, BigDecimal.class, BigDecimal.class, BigDecimal.class,
				int.class };
		MethodExpression methodExpression = MethodExpressionUtil.createMethodExpression(
				"#{investmentController.addInvestment( investmentDialog.initialDeposit, investmentDialog.nominalRate,"
						+ " investmentDialog.monthlyContribution, investmentDialog.bankFees, investmentDialog.termInMonths)}",
				expectedReturnType, expectedParamTypes);
		return methodExpression;
	}

	public MethodExpression getOpenForecast() {
//	MethodExpressionUtil.createMethodExpression(methodExpression, expectedReturnType, expectedParamTypes)
		return null;
	}

	public MethodExpression getSaveForecast() {
		Class<?> expectedReturnType = void.class;
		Class<?>[] expectedParamTypes = {};
		MethodExpression methodExpression = MethodExpressionUtil.createMethodExpression("#{investmentController.update}",
				expectedReturnType, expectedParamTypes);
		return methodExpression;
	}

	public Investment getInstrument() {
		return investment;
	}
}
