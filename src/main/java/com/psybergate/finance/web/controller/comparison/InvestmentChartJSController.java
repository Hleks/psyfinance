package com.psybergate.finance.web.controller.comparison;

import static com.psybergate.finance.util.BigDecimalUtil.percentageToDecimal;
import static com.psybergate.finance.web.util.MethodExpressionUtil.createMethodExpression;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.el.MethodExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.interestbearinginstrument.InterestBearingInstrument;
import com.psybergate.finance.domain.investment.Investment;

@ManagedBean(name = "investmentComparison")
@ViewScoped
public class InvestmentChartJSController extends AbstractChartJSController {


	@Override
	protected Map<String, InterestBearingInstrument> getFromService() {
		instruments = new HashMap<>();
		Customer customer = customerSession.getCustomer();
		customer.getInvestments().forEach(this::addInvestment);
		return instruments;
	}

	private InterestBearingInstrument addInvestment(Investment investment) {
		return instruments.put(investment.getName(), investment);
	}

	@Override
	public MethodExpression getExpression() {
		Class<?>[] expectedParamTypes = { BigDecimal.class, BigDecimal.class, int.class, BigDecimal.class,
				BigDecimal.class };
		MethodExpression methodExpression = createMethodExpression(
				"#{investmentComparison.addInvestmentToSelection(initialDeposit.value, nominalRate.value, termInMonths.value, monthlyContribution.value, bankFees.value)}",
				void.class, expectedParamTypes);
		return methodExpression;
	}

	public void addInvestmentToSelection(BigDecimal initialDeposit, BigDecimal nominalRate, int termInMonths,
			BigDecimal monthlyContribution, BigDecimal bankFees) {
		Investment investment = new Investment(initialDeposit, percentageToDecimal(nominalRate), monthlyContribution, bankFees, termInMonths);
		addInstrumentToSelection(investment);
	}

}
