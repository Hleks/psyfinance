package com.psybergate.finance.web.controller.comparison;

import static com.psybergate.finance.util.BigDecimalUtil.percentageToDecimal;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.el.MethodExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.interestbearinginstrument.InterestBearingInstrument;
import com.psybergate.finance.domain.propertybond.PropertyBond;
import com.psybergate.finance.web.util.MethodExpressionUtil;

@ManagedBean(name = "propertyBondComparison")
@ViewScoped
public class PropertyBondChartJSController extends AbstractChartJSController {

	@Override
	protected Map<String, InterestBearingInstrument> getFromService() {
		instruments = new HashMap<>();
		Customer customer = customerSession.getCustomer();
		customer.getPropertyBonds().forEach(this::addPropertyBond);
		return instruments;
	}

	private InterestBearingInstrument addPropertyBond(PropertyBond propertyBond) {
		return instruments.put(propertyBond.getName(), propertyBond);
	}

	public void addPropertyBondToSelection(BigDecimal nominalRate, BigDecimal purchasePrice, BigDecimal initialDeposit,
			BigDecimal monthlyRepayment, BigDecimal bankFees, int termInMonths) {
		BigDecimal nominalRateAsDecimal = percentageToDecimal(nominalRate);
		BigDecimal initialDepositAsDecimal = percentageToDecimal(initialDeposit);
		PropertyBond propertyBond = new PropertyBond(nominalRateAsDecimal, purchasePrice, initialDepositAsDecimal,
				BigDecimal.ZERO, BigDecimal.ZERO, monthlyRepayment, termInMonths);
		addInstrumentToSelection(propertyBond);
	}

	@Override
	public MethodExpression getExpression() {
		Class<?>[] expectedParamTypes = { BigDecimal.class, BigDecimal.class, BigDecimal.class, BigDecimal.class,
				BigDecimal.class, int.class };
		MethodExpression methodExpression = MethodExpressionUtil.createMethodExpression(
				"#{propertyBondComparison.addPropertyBondToSelection(propertyBondDialog.nominalRate, propertyBondDialog.purchasePrice, propertyBondDialog.initialDepositRate, "
						+ " propertyBondDialog.monthlyRepayment, propertyBondDialog.bankFees, propertyBondDialog.termInMonths)}",
				void.class, expectedParamTypes);
		return methodExpression;
	}

}
