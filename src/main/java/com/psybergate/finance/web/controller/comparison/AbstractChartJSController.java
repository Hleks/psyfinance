package com.psybergate.finance.web.controller.comparison;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.el.MethodExpression;
import javax.inject.Inject;

import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.line.LineChartDataSet;
import org.primefaces.model.charts.line.LineChartModel;
import org.primefaces.model.charts.line.LineChartOptions;
import org.primefaces.model.charts.optionconfig.title.Title;

import com.psybergate.finance.domain.interestbearinginstrument.InterestBearingInstrument;
import com.psybergate.finance.web.Colour;
import com.psybergate.finance.web.controller.CustomerSession;

public abstract class AbstractChartJSController {

	@Inject
	protected CustomerSession customerSession;

	protected LineChartModel lineModel;

	protected List<String> choices = new ArrayList<>();

	protected List<String> selected = new ArrayList<>();

	protected Map<String, InterestBearingInstrument> instruments;

	protected int fooNum;


	@PostConstruct
	public void init() {
		lineModel = new LineChartModel();
		instruments = getFromService();
		instruments.keySet().forEach(k -> choices.add(k));
		setupLineModel();
	}

	protected abstract Map<String, InterestBearingInstrument> getFromService();

	public List<String> getSelected() {
		return selected;
	}

	public void setSelected(List<String> selected) {
		this.selected = selected;
	}

	public List<String> getChoices() {
		return choices;
	}

	public void setupLineModel() {
		ChartData data = new ChartData();

		// Options
		LineChartOptions options = new LineChartOptions();
		Title title = new Title();
		title.setDisplay(true);
		title.setText("Comparison");
		options.setTitle(title);

		lineModel.setOptions(options);
		lineModel.setData(data);
	}

	public void update() {
		updateModel(getSelected());
	}

	public void updateModel(List<String> selected) {
		ChartData data = new ChartData();
		InterestBearingInstrument instrument = null;

		int lineNum = 0;
		for (String instrumentName : selected) {
			instrument = instruments.get(instrumentName);
			if (instrument != null) {
				String colour = getColour(lineNum);
				LineChartDataSet newDataSet = createLineChartDataSet(instrument, instrumentName, colour);
				data.addChartDataSet(newDataSet);
				lineNum++;
			}
		}

		if (instrument != null) {
			List<String> labels = getLabels(instrument);
			data.setLabels(labels);
		}
		lineModel.setData(data);
	}

	private String getColour(int lineNum) {
		Colour[] colours = Colour.values();
		int i = lineNum;
		if(lineNum>=colours.length) {
			i = i%(colours.length);
		}
		return colours[i].getRGB();
	}

	private List<String> getLabels(InterestBearingInstrument instrument) {
		List<String> labels;
		labels = new ArrayList<>();
		int termInMonths = instrument.getTermInMonths();

		for (int i = 1; i <= termInMonths; i++) {
			labels.add(String.valueOf(i));
		}
		return labels;
	}

	public LineChartDataSet createLineChartDataSet(InterestBearingInstrument instrument, String label, String colour) {
		LineChartDataSet dataSet = new LineChartDataSet();
		List<Number> values = new ArrayList<>();

		getChartPoints(instrument, values);

		dataSet.setData(values);
		dataSet.setFill(true);
		dataSet.setLabel(label);
		dataSet.setBorderColor(colour);
		dataSet.setLineTension(0.1);
		return dataSet;
	}

	public LineChartModel getModel() {
		return lineModel;
	}

	public abstract MethodExpression getExpression();

	public void getChartPoints(InterestBearingInstrument instrument, List<Number> values) {
		instrument.getMonths().forEach(month -> values.add(month.getOpening()));
		values.add(instrument.getClosingBalance());
	}

	public void addInstrumentToSelection(InterestBearingInstrument instrument) {
		fooNum++;
		String label = "Foo" + fooNum;
		instruments.put(label, instrument);
		choices.add(label);
		selected.add(label);

		updateModel(selected);
	}

}
