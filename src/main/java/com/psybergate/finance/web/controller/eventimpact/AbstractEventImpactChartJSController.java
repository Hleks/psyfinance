package com.psybergate.finance.web.controller.eventimpact;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.el.MethodExpression;
import javax.inject.Inject;

import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.ChartDataSet;
import org.primefaces.model.charts.line.LineChartDataSet;
import org.primefaces.model.charts.line.LineChartModel;
import org.primefaces.model.charts.line.LineChartOptions;
import org.primefaces.model.charts.optionconfig.title.Title;

import com.psybergate.finance.domain.interestbearinginstrument.InterestBearingInstrument;
import com.psybergate.finance.web.controller.CustomerSession;

public abstract class AbstractEventImpactChartJSController {
	
	@Inject
	protected CustomerSession customerSession;
	
	protected LineChartModel lineModel;
	
	protected LineChartDataSet lineChartImpactOfEvents = new LineChartDataSet();
	
	protected List<String> savedInstruments = new ArrayList<>();

	protected List<String> choices = new ArrayList<>();

	protected String selected = "";
	
	protected Map<String, InterestBearingInstrument> instruments;
	
	protected InterestBearingInstrument instrumentImpactedByEvents;

	protected int fooNum;
	
	protected String withoutEventsColour = "rgb(75, 192, 192)";
	
	protected String withEventsColour = "rgb(227, 75, 57)";

	@PostConstruct
	public void init() {
		lineModel = new LineChartModel();
		instruments = getFromService();
		instruments.keySet().forEach(k -> choices.add(k));
		instruments.keySet().forEach(k -> savedInstruments.add(k));
		setupLineModel();
	}

	protected abstract Map<String, InterestBearingInstrument> getFromService();
	
	protected abstract String getType();

	public String getSelected() {
		return selected;
	}

	public void setSelected(String selected) {
		this.selected = selected;
	}

	public List<String> getChoices() {
		return choices;
	}

	public void setupLineModel() {
		ChartData data = new ChartData();

		// Options
		LineChartOptions options = new LineChartOptions();
		Title title = new Title();
		title.setDisplay(true);
		title.setText("Comparison");
		options.setTitle(title);

		lineModel.setOptions(options);
		lineModel.setData(data);
	}

	public void update() {
		updateSelectedInstrument(getSelected());
	}

	public void updateSelectedInstrument(String selected) {
		ChartData data = new ChartData();
		InterestBearingInstrument instrument = null;

			instrument = instruments.get(selected);
			
			if(instrument != null) {
				LineChartDataSet originalDataSet = null;
				
				if(savedInstruments.contains(selected))
				{
					originalDataSet = createLineChartDataSet(createInstrumentImpactedByEvents(instrument), selected+" before Events", withoutEventsColour);
					instrumentImpactedByEvents = instrument;
					lineChartImpactOfEvents = createLineChartDataSet(instrument, selected, withEventsColour);
					
				}else {
					originalDataSet = createLineChartDataSet(instrument, selected, withoutEventsColour);
					instrumentImpactedByEvents = createInstrumentImpactedByEvents(instrument);
					lineChartImpactOfEvents = createLineChartDataSet(instrumentImpactedByEvents, selected + " After Events", withoutEventsColour);
				}
				
				data.addChartDataSet(originalDataSet);

				data.addChartDataSet(lineChartImpactOfEvents);

					List<String> labels = getLabels(instrument);
					data.setLabels(labels);			
					lineModel.setData(data);
		}
	}

	private List<String> getLabels(InterestBearingInstrument instrument) {
		List<String> labels;
		labels = new ArrayList<>();
		int termInMonths = instrument.getTermInMonths();

		for (int i = 1; i <= termInMonths; i++) {
			labels.add(String.valueOf(i));
		}
		return labels;
	}

	public LineChartDataSet createLineChartDataSet(InterestBearingInstrument instrument, String label, String colour) {
		LineChartDataSet dataSet = new LineChartDataSet();
		List<Number> values = new ArrayList<>();

		getChartPoints(instrument, values);
		
		dataSet.setData(values);
		dataSet.setFill(true);
		dataSet.setLabel(label);
		dataSet.setBorderColor(colour);
		dataSet.setLineTension(0.1);
		return dataSet;
	}

	public LineChartModel getModel() {
		return lineModel;
	}
	
	public abstract MethodExpression getExpression();
	
	public void getChartPoints(InterestBearingInstrument instrument, List<Number> values) {
		instrument.getMonths().forEach(month -> values.add(month.getOpening()));
		values.add(instrument.getClosingBalance());
	}

	public void addInstrumentToSelection(InterestBearingInstrument instrument) {
		fooNum++;
		String label = getType() + fooNum;
		instruments.put(label, instrument);
		choices.add(label);
		selected= label;

		updateSelectedInstrument(selected);
	}
	
	public void updateAfterEvent() {
		lineChartImpactOfEvents = createLineChartDataSet(instrumentImpactedByEvents, selected + " After Events", "rgb(227, 75, 57)");
		ChartData data = lineModel.getData();
		List<ChartDataSet> dataSets = data.getDataSet();
		dataSets.set(1, lineChartImpactOfEvents);
	}

	public abstract InterestBearingInstrument createInstrumentImpactedByEvents(InterestBearingInstrument original);
	
	
}
