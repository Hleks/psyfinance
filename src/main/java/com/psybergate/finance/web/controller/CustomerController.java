package com.psybergate.finance.web.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.event.SelectEvent;

import com.psybergate.finance.PsyfinanceException;
import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.investment.Investment;
import com.psybergate.finance.domain.propertybond.PropertyBond;

@ManagedBean(name = "customerController")
@ViewScoped
public class CustomerController {

	@Inject
	private CustomerSession customerSession;

	private Customer customer;

	private Investment selectedInvestmentForecast;

	private List<Investment> selectedInvestmentForecasts;

	private PropertyBond selectedPropertyBondForecast;

	private List<PropertyBond> selectedPropertyBondForecasts;

	@PostConstruct
	public void init() {
		customer = customerSession.getCustomer();
//		customer = customerService.findCustomer(customerSession.getCustomerName());
	}

	public Customer getSelectedCustomer() {
		return customer;
	}

	public void setSelectedCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getFullName() {
		return customer.getFullName();
	}

	public void setFullName(String fullName) {
		customer.setFullName(fullName);
	}

	public void setInvestmentForecast(Investment selectedInvestment) {
		this.selectedInvestmentForecast = selectedInvestment;
	}

	public List<Investment> getSelectedInvestmentForecasts() {
		return selectedInvestmentForecasts;
	}

	public void setSelectedInvestmentForecasts(List<Investment> selectedInvestments) {
		this.selectedInvestmentForecasts = selectedInvestments;
	}

	public void setSelectedPropertyBondForecast(PropertyBond selectedPropertyBondForecast) {
		this.selectedPropertyBondForecast = selectedPropertyBondForecast;
	}

	public PropertyBond getSelectedPropertyBondForecast() {
		return selectedPropertyBondForecast;
	}

	public List<PropertyBond> getSelectedPropertyBondForecasts() {
		return selectedPropertyBondForecasts;
	}

	public void setSelectedPropertyBondForecasts(List<PropertyBond> selectedPropertyBondForecasts) {
		this.selectedPropertyBondForecasts = selectedPropertyBondForecasts;
	}

	public Investment getSelectedInvestmentForecast() {
		return selectedInvestmentForecast;
	}

	public void setSelectedInvestmentForecast(Investment selectedInvestmentForecast) {
		this.selectedInvestmentForecast = selectedInvestmentForecast;
	}

	public void onInvestmentRowSelect(SelectEvent event) {
		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("investments.xhtml?id=" + selectedInvestmentForecast.getId());
		} catch ( IOException e ) {
			throw new PsyfinanceException(e);
		}
	}

	public void onPropertyBondRowSelect(SelectEvent event) {
		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("propertybonds.xhtml?id=" + selectedPropertyBondForecast.getId());
		} catch ( IOException e ) {
			throw new PsyfinanceException(e);
		}
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getFinalMonthTitle(PropertyBond propertyBond) {
		if (propertyBond.getFinalPaymentMonth() < 0) return "";
		return "Paid off in month";
	}

	public String getFinalMonthBody(PropertyBond propertyBond) {
		if (propertyBond.getFinalPaymentMonth() < 0) return "Not paid off.";
		return String.valueOf(propertyBond.getFinalPaymentMonth());
	}

	public String getForecastSummaryForInvestments(Investment investment) {
		return getInvestmentName(investment) + " (Closing Balance: " + getInvestmentClosingBalance(investment) + ", Cumalitive Interest: "
				+ ", " + getInvestmentCummalitiveInterest(investment) + ", Effective Rate: " + getInvestmentEffectiveRate(investment)
				+ ", Total Bank Fees: " + getInvestmentBankingFees(investment) + ")";
	}

	public BigDecimal getInvestmentBankingFees(Investment investment) {
		return investment.getTotalBankFees();
	}

	public BigDecimal getInvestmentEffectiveRate(Investment investment) {
		return investment.getEffectiveRate();
	}

	public BigDecimal getInvestmentCummalitiveInterest(Investment investment) {
		return investment.getCumalitiveInterest();
	}

	public BigDecimal getInvestmentClosingBalance(Investment investment) {
		return investment.getClosingBalance();
	}

	public String getInvestmentName(Investment investment) {
		return investment.getName();
	}

	public String getForecastSummaryForPropertyBonds(PropertyBond propertyBond) {
		return getPropertyBondName(propertyBond) + " (Total Repayment: " + getPropertyBondTotalBondCost(propertyBond) + ", "
				+ getFinalMonthTitle(propertyBond) + ": " + getFinalMonthBody(propertyBond) + ", Total Interest Earned:"
				+ getPropertyBondCumalitiveInterest(propertyBond) + ", Total Cash Required Up Front: "
				+ getPropertyBondTotalCashRequiredUpfornt(propertyBond) + ")";
	}

	public BigDecimal getPropertyBondTotalCashRequiredUpfornt(PropertyBond propertyBond) {
		return propertyBond.getTotalCashRequiredUpfront();
	}

	public BigDecimal getPropertyBondCumalitiveInterest(PropertyBond propertyBond) {
		return propertyBond.getCumalitiveInterest();
	}

	public BigDecimal getPropertyBondTotalBondCost(PropertyBond propertyBond) {
		return propertyBond.getTotalBondCost();
	}

	public String getPropertyBondName(PropertyBond propertyBond) {
		return propertyBond.getName();
	}

}
