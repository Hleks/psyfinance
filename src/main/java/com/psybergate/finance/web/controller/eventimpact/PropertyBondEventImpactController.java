package com.psybergate.finance.web.controller.eventimpact;

import static com.psybergate.finance.util.BigDecimalUtil.percentageToDecimal;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.el.MethodExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.interestbearinginstrument.InterestBearingInstrument;
import com.psybergate.finance.domain.propertybond.PropertyBond;
import com.psybergate.finance.web.util.MethodExpressionUtil;

@ManagedBean(name = "propertyBondEventImpact")
@ViewScoped
public class PropertyBondEventImpactController extends AbstractEventImpactChartJSController {

	@Override
	protected Map<String, InterestBearingInstrument> getFromService() {
		instruments = new HashMap<>();
		Customer customer = customerSession.getCustomer();
		customer.getPropertyBonds().forEach(this::addPropertyBond);
		return instruments;
	}

	private InterestBearingInstrument addPropertyBond(PropertyBond propertyBond) {
		return instruments.put(propertyBond.getName(), propertyBond);
	}

	public void addPropertyBondToSelection(BigDecimal nominalRate, BigDecimal purchasePrice, BigDecimal initialDeposit,
			BigDecimal monthlyRepayment, BigDecimal bankFees, int termInMonths) {
		BigDecimal nominalRateAsDecimal = percentageToDecimal(nominalRate);
		BigDecimal initialDepositAsDecimal = percentageToDecimal(initialDeposit);
		PropertyBond propertyBond = new PropertyBond(nominalRateAsDecimal, purchasePrice, initialDepositAsDecimal,
				BigDecimal.ZERO, BigDecimal.ZERO, monthlyRepayment, termInMonths);
		addInstrumentToSelection(propertyBond);
	}

	@Override
	public MethodExpression getExpression() {
		Class<?>[] expectedParamTypes = { BigDecimal.class, BigDecimal.class, BigDecimal.class, BigDecimal.class,
				BigDecimal.class, int.class };
		MethodExpression methodExpression = MethodExpressionUtil.createMethodExpression(
				"#{propertyBondEventImpact.addPropertyBondToSelection(propertyBondDialog.nominalRate, propertyBondDialog.purchasePrice, propertyBondDialog.initialDepositRate,"
						+ " propertyBondDialog.monthlyRepayment, propertyBondDialog.bankFees, propertyBondDialog.termInMonths)}",
				void.class, expectedParamTypes);
		return methodExpression;
	}

	@Override
	public InterestBearingInstrument createInstrumentImpactedByEvents(InterestBearingInstrument original) {
		return instrumentImpactedByEvents = new PropertyBond((PropertyBond) original);
	}

	public void addRateChangeEvent(int rateChangeMonth, BigDecimal rateChange) {
		((PropertyBond) instrumentImpactedByEvents).setNominalRateFor(rateChangeMonth, percentageToDecimal(rateChange));
		updateAfterEvent();
	}

	public void addDepositEvent(int depositMonth, BigDecimal deposit) {
		((PropertyBond) instrumentImpactedByEvents).addDepositFor(depositMonth, deposit);
		updateAfterEvent();
	}

	public void addWithdrawalEvent(int withdrawalMonth, BigDecimal withdrawal) {
		((PropertyBond) instrumentImpactedByEvents).addWithdrawalFor(withdrawalMonth, withdrawal);
		updateAfterEvent();
	}

	public void addBankFeesEvent(int bankingFeesMonth, BigDecimal bankingFees) {
		((PropertyBond) instrumentImpactedByEvents).addBankFeesFor(bankingFeesMonth, bankingFees);
		updateAfterEvent();
	}

	public void addMonthlyRepaymentChangeEvent(int repaymentMonth, BigDecimal repaymentFees) {
		((PropertyBond) instrumentImpactedByEvents).addMonthlyRepaymentFor(repaymentMonth, repaymentFees);
		updateAfterEvent();
	}

	public void useMinimumRepayment(int startMonth, int endMonth) {
		((PropertyBond) instrumentImpactedByEvents).useMinimumRepayment(startMonth, endMonth);
		updateAfterEvent();
	}
	
	public MethodExpression getMonthlyRepaymentDialogEvent() {
		return MethodExpressionUtil.createEventDialogMethodExpression("propertyBondEventImpact" , "addMonthlyRepaymentChangeEvent");
	}
	
	public Map<String, MethodExpression> getEvents(){
		Map<String, MethodExpression> eventMethods = new HashMap<>();
		
		eventMethods.put("monthlyRepayment", MethodExpressionUtil.createEventDialogMethodExpression("propertyBondEventImpact" , "addMonthlyRepaymentChangeEvent"));
		eventMethods.put("bankFees", MethodExpressionUtil.createEventDialogMethodExpression("propertyBondEventImpact" , "addBankFeesEvent"));
		
		return eventMethods;
	}

	@Override
	protected String getType() {
		return "Property Bond";
	}

}
