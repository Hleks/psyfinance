package com.psybergate.finance.web.controller.eventimpact;

import static com.psybergate.finance.util.BigDecimalUtil.percentageToDecimal;
import static com.psybergate.finance.web.util.MethodExpressionUtil.createMethodExpression;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.el.MethodExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.ChartDataSet;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.interestbearinginstrument.InterestBearingInstrument;
import com.psybergate.finance.domain.investment.Investment;
import com.psybergate.finance.web.util.MethodExpressionUtil;

@ManagedBean(name = "investmentEventImpact")
@ViewScoped
public class InvestmentEventImpactController extends AbstractEventImpactChartJSController {

	@Override
	protected Map<String, InterestBearingInstrument> getFromService() {
		instruments = new HashMap<>();
		Customer customer = customerSession.getCustomer();
		customer.getInvestments().forEach(this::addInvestment);
		return instruments;
	}
	
	private InterestBearingInstrument addInvestment(Investment investment) {
		return instruments.put(investment.getName(), investment);
	}
	
	@Override
	public MethodExpression getExpression() {
		Class<?>[] expectedParamTypes = { BigDecimal.class, BigDecimal.class, int.class, BigDecimal.class,
				BigDecimal.class };
		MethodExpression methodExpression = createMethodExpression(
				"#{investmentEventImpact.addInvestmentToSelection(initialDeposit.value, nominalRate.value, termInMonths.value, monthlyContribution.value, bankFees.value)}",
				void.class, expectedParamTypes);
		return methodExpression;
	}

	public void addInvestmentToSelection(BigDecimal initialDeposit, BigDecimal nominalRate, int termInMonths,
			BigDecimal monthlyContribution, BigDecimal bankFees) {
		Investment investment = new Investment(initialDeposit, percentageToDecimal(nominalRate), monthlyContribution,
				bankFees, termInMonths);
		addInstrumentToSelection(investment);
	}

	public void updateAfterEvent() {
		lineChartImpactOfEvents = createLineChartDataSet(instrumentImpactedByEvents, selected + " After Events",
				"rgb(227, 75, 57)");
		ChartData data = lineModel.getData();
		List<ChartDataSet> dataSets = data.getDataSet();
		dataSets.set(1, lineChartImpactOfEvents);
//		data.addChartDataSet(lineChartImpactOfEvents);
	}

	private Investment cloneInvestment(Investment original) {
		return new Investment(original.getPrincipal(), original.getNominalRate(), 
				original.getMonthlyContribution(), original.getBankFees(), original.getTermInMonths());
	}


	public InterestBearingInstrument createInstrumentImpactedByEvents(InterestBearingInstrument original) {
		return instrumentImpactedByEvents = cloneInvestment((Investment) original);
	}

	public void addRateChangeEvent(int rateChangeMonth, BigDecimal rateChange) {
		((Investment) instrumentImpactedByEvents).setNominalRateFor(rateChangeMonth, percentageToDecimal(rateChange));
		updateAfterEvent();
	}

	public void addDepositEvent(int depositMonth, BigDecimal deposit) {
		((Investment) instrumentImpactedByEvents).deposit(depositMonth, deposit.doubleValue());
		updateAfterEvent();
	}

	public void addWithdrawalEvent(int withdrawalMonth, BigDecimal withdrawal) {
		((Investment) instrumentImpactedByEvents).withdraw(withdrawalMonth, withdrawal.doubleValue());
		updateAfterEvent();
	}

	public void addBankFeesEvent(int bankingFeesMonth, BigDecimal bankingFees) {
		((Investment) instrumentImpactedByEvents).addBankFeesFor(bankingFeesMonth, bankingFees);
		updateAfterEvent();
	}
	
	public void addMonthlyContributionChangeEvent(int contributionMonth, BigDecimal contributionAmount) {
		((Investment) instrumentImpactedByEvents).addMonthlyContributionFor(contributionMonth, contributionAmount);
		updateAfterEvent();
	}

	public MethodExpression getMonthlyContributionDialogEvent() {
		return MethodExpressionUtil.createEventDialogMethodExpression("investmentEventImpact" , "addMonthlyContributionChangeEvent");
	}
	
	public Map<String, MethodExpression> getEvents(){
		Map<String, MethodExpression> eventMethods = new HashMap<>();
		
		eventMethods.put("monthlyContribution", MethodExpressionUtil.createEventDialogMethodExpression("investmentEventImpact" , "addMonthlyContributionChangeEvent"));
		eventMethods.put("bankFees", MethodExpressionUtil.createEventDialogMethodExpression("investmentEventImpact" , "addBankFeesEvent"));
		
		return eventMethods;
	}

	@Override
	protected String getType() {
		return "Investments";
	}

}
