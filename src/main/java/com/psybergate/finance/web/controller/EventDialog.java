package com.psybergate.finance.web.controller;

import java.math.BigDecimal;

import javax.el.ELContext;
import javax.el.MethodExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@RequestScoped
public class EventDialog {

	private BigDecimal amount = BigDecimal.ZERO;
	
	private int month = 0;
	
	private MethodExpression submitAction;
	
	
	public EventDialog() {
	}
	
	
	public BigDecimal getAmount() {
		return amount;
	}

	
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	
	public int getMonth() {
		return month;
	}

	
	public void setMonth(int month) {
		this.month = month;
	}

	
	public void submitAction() {
    ELContext elContext = FacesContext.getCurrentInstance().getELContext(); 
		Object[] params = {month, amount};
		submitAction.invoke(elContext, params);
	}

	
	public void setSubmitAction(MethodExpression submitAction) {
		this.submitAction = submitAction;
	}
	
	
}
