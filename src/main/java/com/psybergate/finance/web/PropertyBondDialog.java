package com.psybergate.finance.web;

import static com.psybergate.finance.util.BigDecimalUtil.ZERO;
import static com.psybergate.finance.util.BigDecimalUtil.decimalToPercentage;

import java.math.BigDecimal;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.psybergate.finance.domain.propertybond.PropertyBond;

@ManagedBean(name = "propertyBondDialog")
@RequestScoped
public class PropertyBondDialog {

	private BigDecimal purchasePrice = ZERO;

	private BigDecimal nominalRate = ZERO;

	private BigDecimal initialDepositRate = ZERO;

	private int termInMonths = 1;

	private BigDecimal monthlyRepayment = ZERO;

	private BigDecimal bankFees = ZERO;

	private BigDecimal bondCostsRate = ZERO;

	private BigDecimal legalFeesRate = ZERO;
	
	
	public PropertyBondDialog() {
		// TODO Auto-generated constructor stub
	}
	
	public void setPropertyBond(PropertyBond propertyBond) {
		
		if(propertyBond!=null) {			
		purchasePrice = propertyBond.getPurchasePrice();
		nominalRate= decimalToPercentage(propertyBond.getNominalRate());
		initialDepositRate = decimalToPercentage(propertyBond.getInitialDepositRate());
		termInMonths = propertyBond.getTermInMonths();
		monthlyRepayment = propertyBond.getMonthlyRepayment();
		bankFees = propertyBond.getBankFees();
		bondCostsRate = decimalToPercentage(propertyBond.getBondCostsPercentage());
		legalFeesRate = decimalToPercentage(propertyBond.getLegalFeesPercentage());
		}
	}

	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(BigDecimal purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public BigDecimal getInitialDepositRate() {
		return initialDepositRate;
	}

	public void setInitialDepositRate(BigDecimal initialDepositRate) {
		this.initialDepositRate = initialDepositRate;
	}

	public BigDecimal getMonthlyRepayment() {
		return monthlyRepayment;
	}

	public void setMonthlyRepayment(BigDecimal monthlyRepayment) {
		this.monthlyRepayment = monthlyRepayment;
	}

	public BigDecimal getBankFees() {
		return bankFees;
	}

	public void setBankFees(BigDecimal bankFees) {
		this.bankFees = bankFees;
	}

	public BigDecimal getBondCostsRate() {
		return bondCostsRate;
	}

	public void setBondCostsRate(BigDecimal bondCostsRate) {
		this.bondCostsRate = bondCostsRate;
	}

	public BigDecimal getLegalFeesRate() {
		return legalFeesRate;
	}

	public void setLegalFeesRate(BigDecimal legalFeesRate) {
		this.legalFeesRate = legalFeesRate;
	}

	public int getTermInMonths() {
		return termInMonths;
	}

	public void setTermInMonths(int termInMonths) {
		this.termInMonths = termInMonths;
	}

	
	public BigDecimal getNominalRate() {
		return nominalRate;
	}

	
	public void setNominalRate(BigDecimal nominalRate) {
		this.nominalRate = nominalRate;
	}

}
