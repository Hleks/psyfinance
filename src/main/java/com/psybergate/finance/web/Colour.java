package com.psybergate.finance.web;

public enum Colour {

	AQUA("rgb(0, 192, 239)"), RED("rgb(227, 75, 57)"), YELLOW("rgb(243, 156, 18)"),  GREEN("rgb(0,166,90)"),
	PURPLE("rgb(96, 92, 168)"), 	BLUE("rgb(75, 192, 192)"), TEAL("rgb(0,150,136)"), BLACK("rgb(255,255,255)");

	String rgb;

	private Colour(String rgb) {
		this.rgb = rgb;
	}

	public String getRGB() {
		return rgb;
	}

	@Override
	public String toString() {
		return rgb;
	}

}
