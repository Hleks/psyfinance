package com.psybergate.finance.service.propertybond;

import java.math.BigDecimal;

import javax.ejb.Local;

import com.psybergate.finance.domain.propertybond.PropertyBond;

@Local
public interface PropertyBondService {

	BigDecimal calculateTotalInterestCharged(PropertyBond propertyBond);

	BigDecimal calculateTotalPaid(PropertyBond propertyBond);

	BigDecimal calculateTransferTax(PropertyBond propertyBond);

	BigDecimal calculateTotalBondCosts(PropertyBond propertyBond);

	void addForecast(PropertyBond propertyBond);

	PropertyBond getPropertyBond(long propertyBondID);

}
