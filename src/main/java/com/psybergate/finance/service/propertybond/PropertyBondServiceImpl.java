package com.psybergate.finance.service.propertybond;

import java.math.BigDecimal;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.psybergate.finance.domain.propertybond.PropertyBond;
import com.psybergate.finance.resource.PropertyBondResource;

@Stateless
public class PropertyBondServiceImpl implements PropertyBondService {

	@Inject
	private PropertyBondResource propertyBondResource;

	@Override
	public BigDecimal calculateTotalInterestCharged(PropertyBond propertyBond) {
		return propertyBond.getCumalitiveInterest();
	}

	@Override
	public BigDecimal calculateTotalPaid(PropertyBond propertyBond) {
		return propertyBond.getTotalBondCost();
	}

	@Override
	public BigDecimal calculateTransferTax(PropertyBond propertyBond) {
		return propertyBond.getTransferTax();
	}

	@Override
	public BigDecimal calculateTotalBondCosts(PropertyBond propertyBond) {
		return propertyBond.getBondCosts();
	}

	@Override
	public void addForecast(PropertyBond propertyBond) {
		propertyBondResource.save(propertyBond);
	}

	public PropertyBond getPropertyBond(long propertyBondID) {
		PropertyBond propertyBond = propertyBondResource.find(propertyBondID);
		propertyBond.updateMonths();
		return propertyBond;
	}
}
