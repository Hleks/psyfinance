package com.psybergate.finance.service.customer;

import java.util.List;
import java.util.Set;

import javax.ejb.Local;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.investment.Investment;
import com.psybergate.finance.domain.propertybond.PropertyBond;

@Local
public interface CustomerService {

	void addCustomer(Customer customer);
	
	void updateCustomer(Customer customer);

	Set<Customer> getCustomers();

	Customer findCustomer(String email);

	void addForecast(Customer customer, Investment investment);
	
	void addForecast(Customer customer, PropertyBond propertyBond);

	List<Investment> getInvestmentsFor(Customer customer);

	Customer getCustomer(Long customerNum);
}
