package com.psybergate.finance.service.customer;

import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.investment.Investment;
import com.psybergate.finance.domain.propertybond.PropertyBond;
import com.psybergate.finance.resource.CustomerResource;
import com.psybergate.finance.service.investment.InvestmentService;
import com.psybergate.finance.service.propertybond.PropertyBondService;

@Stateless
public class CustomerServiceImpl implements CustomerService {

	@Inject
	private CustomerResource customerResource;

	@Inject
	private InvestmentService investmentService;

	@Inject
	private PropertyBondService propertyBondService;

	@Override
	public void addCustomer(Customer customer) {
		customerResource.save(customer);
	}

	@Override
	public Set<Customer> getCustomers() {
		return customerResource.findAll();
	}

	@Override
	public Customer getCustomer(Long customerNum) {
		return customerResource.find(customerNum);
	}

	@Override
	public Customer findCustomer(String email) {
		Customer customer = customerResource.findByUnique(email);
		customer.getInvestments().forEach(Investment::updateMonths);
		customer.getPropertyBonds().forEach(PropertyBond::updateMonths);
		return customer;
	}

	@Override
	public void updateCustomer(Customer customer) {
		customerResource.update(customer);
	}

	@Override
	public void addForecast(Customer customer, Investment investment) {
		investment.setCustomer(customer);
		investmentService.addInvestment(investment);
		customer.addForecast(investment);
	}

	@Override
	public void addForecast(Customer customer, PropertyBond propertyBond) {
		propertyBond.setCustomer(customer);
		propertyBondService.addForecast(propertyBond);
		customer.addForecast(propertyBond);
	}

	@Override
	public List<Investment> getInvestmentsFor(Customer customer) {
		return customer.getInvestments();
	}

}