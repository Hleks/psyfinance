package com.psybergate.finance.service.investment;

import java.math.BigDecimal;

import javax.ejb.Local;

import com.psybergate.finance.domain.investment.Investment;

@Local
public interface InvestmentService {

	BigDecimal calculateTotalInterestEarned(Investment investment);

	BigDecimal calculateClosingBalance(Investment investment);

	void deposit(Investment investment, int monthNum, double amount);

	void withdraw(Investment investment, int monthNum, double amount);

	void setNominalRateFor(Investment investment, int monthNum, BigDecimal bigDecimal);

	void addInvestment(Investment investment);

	Investment getInvestment(String investmentName);

	Investment getInvestment(long id);

}
