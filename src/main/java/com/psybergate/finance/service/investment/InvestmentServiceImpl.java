package com.psybergate.finance.service.investment;

import java.math.BigDecimal;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.psybergate.finance.domain.investment.Investment;
import com.psybergate.finance.resource.InvestmentResource;

@Stateless
public class InvestmentServiceImpl implements InvestmentService {

	@Inject
	private InvestmentResource investmentResource;

	public InvestmentServiceImpl() {
		super();
	}

	@Override
	public BigDecimal calculateTotalInterestEarned(Investment investment) {
		return investment.getInterestFor(investment.getMonths().size());
	}

	@Override
	public BigDecimal calculateClosingBalance(Investment investment) {
		return investment.getClosingBalance();
	}

	@Override
	public void deposit(Investment investment, int monthNum, double amount) {
		investment.deposit(monthNum, amount);
	}

	@Override
	public void withdraw(Investment investment, int monthNum, double amount) {
		investment.withdraw(monthNum, amount);
	}

	public void setNominalRateFor(Investment investment, int monthNum, BigDecimal nominalRate) {
		investment.setNominalRateFor(monthNum, nominalRate);
	}

	@Override
	public void addInvestment(Investment investment) {
		investmentResource.save(investment);
	}

	@Override
	public Investment getInvestment(String investmentName) {
		Investment investment = investmentResource.findByUnique(investmentName);
		investment.updateMonths();
		return investment;
	}

	@Override
	public Investment getInvestment(long id) {
		Investment investment = investmentResource.find(id);
		investment.updateMonths();
		return investment;
	}

}
