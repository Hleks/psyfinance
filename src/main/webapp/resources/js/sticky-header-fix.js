   $('body').on('expanded.pushMenu collapsed.pushMenu', function() {

    var stickyContainer = $('<div class="ui-datatable ui-datatable-sticky ui-widget"><table></table></div>');

    stickyContainer.hide();


    setTimeout(function() {
   		 $(window).trigger('resize');

   		 var $this = PF('tableWidget');
        if ($this) {
            var stickyContainer = $this.jq.find('.ui-datatable-sticky');
            if(stickyHeaderTop == 0) {
                stickyHeaderTop = stickyContainer.css('top');
            }
            if(stickyHeaderLeft == 0) {
                stickyHeaderLeft = stickyContainer.css('left');
            }

            var table = this.thead.parent();
        	var offset = table.offset();

            stickyContainer.css('top', stickyHeaderTop);
            stickyContainer.css('left', offset.left);
        }
    }, 350);
} 	
);

    if(PrimeFaces.widget.DataTable) {
    //@Override
    PrimeFaces.widget.DataTable.prototype.setupStickyHeader = function() {
         var table = this.thead.parent(),
        offset = table.offset(),
        win = $(window),
        $this = this;

        this.stickyContainer = $('<div class="ui-datatable ui-datatable-sticky ui-widget"><table></table></div>');
        this.clone = this.thead.clone(false);
        this.stickyContainer.children('table').append(this.thead);
        table.prepend(this.clone);

        this.stickyContainer.css({
            position: 'absolute',
            width: table.outerWidth(),
            top: offset.top,
            left: offset.left,
            'z-index': ++PrimeFaces.zindex
        });

        this.jq.prepend(this.stickyContainer);

        if(this.cfg.resizableColumns) {
            this.relativeHeight = 0;
        }

        PrimeFaces.utils.registerScrollHandler(this, 'scroll.' + this.id, function() {
            var scrollTop = win.scrollTop(),
            tableOffset = table.offset();

            if(scrollTop > tableOffset.top) {
                $this.stickyContainer.css({
                                        'position': 'fixed',
                                        'top': '0px'
                                    })
                                    .addClass('ui-shadow ui-sticky');

                if($this.cfg.resizableColumns) {
                    $this.relativeHeight = scrollTop - tableOffset.top;
                }

                if(scrollTop >= (tableOffset.top + $this.tbody.height()))
                    $this.stickyContainer.hide();
                else
                    $this.stickyContainer.show();
            }
            else {
                $this.stickyContainer.css({
                                        'position': 'absolute',
                                        'top': tableOffset.top
                                    })
                                    .removeClass('ui-shadow ui-sticky');

                if($this.stickyContainer.is(':hidden')) {
                    $this.stickyContainer.show();
                }

                if($this.cfg.resizableColumns) {
                    $this.relativeHeight = 0;
                }
            }
        });

        PrimeFaces.utils.registerResizeHandler(this, 'resize.sticky-' + this.id, null, function() {
            $this.stickyContainer.width(table.outerWidth());
            $this.stickyContainer.css('left',table.offset().left);
            $this.stickyContainer.css('top',table.offset().top);
        });

        //filter support
        this.clone.find('.ui-column-filter').prop('disabled', true);
		};
}
